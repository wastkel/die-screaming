﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Mirror;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

public class RoomManager : NetworkBehaviour
{
    public List<Node> nodes;
    public List<Node> nodesPass;
    [SerializeField] private List<PropsParams> propsPlaceHolder;
    public List<RoomPreset> roomPresets = new();
    [SerializeField] private List<RespawnPlaceholder> respawnsPlaceHolders;
    public GameObject RespawnSheetPrefab;
    //[HideInInspector] public List<uint> respawns;
    [HideInInspector] public LocationGenerator location;
    PrefabLightmapData lightmapData;

    [Space] [Header("Debug")]
    public MainDoor mainDoor;
    [SyncVar] public bool lockedMainDoor;
    [SyncVar] public int idKeyMainDoor;
    //public List<Door> containerDoors = new ();
    public RoomPreset currentPreset;

    public RoomPreset CurrentPreset
    {
        get
        {
            if (roomPresets.Count <= 0) return null;
            var random = new System.Random(seed);
            var rndInxProps = random.Next(0, roomPresets.Count);
            currentPreset = roomPresets[rndInxProps];
            return currentPreset;
        }
    }

    [SyncVar] public int seed;
    //public SyncList<int> containerDoors = new();
    //private SyncDictionary<byte, int> containerDoors = new();
    //public SyncList<byte> containerDoors = new();

    private void Awake()
    {
        mainDoor = GetComponentInChildren<MainDoor>();
        roomPresets = GetComponentsInChildren<RoomPreset>(true).ToList();
        lightmapData = GetComponent<PrefabLightmapData>();
    }

    [Server]
    public void SetSeed(int _seed)
    {
        seed = _seed;
        if (CurrentPreset)
        {
            respawnsPlaceHolders = CurrentPreset.GetComponentsInChildren<RespawnPlaceholder>().ToList();
            //спавн респов
            foreach (var respawnsPlaceHolder in respawnsPlaceHolders)
            {
                var respawnGo = Instantiate(RespawnSheetPrefab, respawnsPlaceHolder.transform.position,
                    respawnsPlaceHolder.transform.rotation);
                NetworkServer.Spawn(respawnGo);
                respawnGo.SetActive(false);
                location.respawns.Add(respawnGo.GetComponent<RespawnSheet>().netId);
            }

            RpcSetFurniture();
        }
    }
    
    [ClientRpc]
    public void RpcSetFurniture()
    {
        var random = new System.Random(seed);

        //спавн фурнитуры
        if (roomPresets.Count > 0)
        {
            var rndInxProps = random.Next(0, roomPresets.Count);
            currentPreset = roomPresets[rndInxProps];
            currentPreset.gameObject.SetActive(true);
            if (lightmapData)
                lightmapData.Init(rndInxProps);
        }
    }
    
    [Serializable]
    struct PropsParams
    {
        [SerializeField] string Name;
        [SerializeField] public int maxCount;
        [SerializeField] public List<GameObject> propsPrefab;
        [SerializeField] public List<GameObject> anchor;
    }
}