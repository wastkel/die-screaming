using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class EyeAdaptation : MonoBehaviour
{
    public Volume postProcessingVolume; // Ваш Volume
    private Tonemapping tonemapping;
    public float adaptationSpeed = 1.0f; // Скорость адаптации
    private float targetExposure = 1.0f;
    private float currentExposure = 1.0f;

    void Start()
    {
        if (postProcessingVolume.profile.TryGet(out tonemapping))
        {
            targetExposure = 1.0f; // Исходное значение экспозиции
        }
    }

    void Update()
    {
        float averageBrightness = CalculateSceneBrightness(); // Здесь ваш метод для анализа яркости
        targetExposure = Mathf.Clamp(averageBrightness, 0.5f, 2.0f); // Диапазон значений экспозиции

        currentExposure = Mathf.Lerp(currentExposure, targetExposure, Time.deltaTime * adaptationSpeed);

        if (tonemapping != null)
        {
            // Симуляция изменения экспозиции через тонмаппинг
        }
    }

    float CalculateSceneBrightness()
    {
        // Реализация анализа сцены, например, через RenderTexture
        return 1.0f; // Временно
    }
}
