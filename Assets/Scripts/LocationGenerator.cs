﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Reflection;
using Mirror;
using Unity.Mathematics;
using UnityEngine.Serialization;
using Object = System.Object;
using Random = UnityEngine.Random;

public class LocationGenerator : NetworkBehaviour
{
    //Data
    [Space, Header("Prefabs")] 
    [SerializeField] private List<GameObject> spawnRoomPrefab;
    [SerializeField] private List<GameObject> famousPartPrefab;
    // [SerializeField] private List<GameObject> uniqueRoomPrefab;
    // [HideInInspector] public List<RoomManager> uniqueRoom = new ();
    [SerializeField] private List<GameObject> monsterRoomPrefab;
    [SerializeField] private List<GameObject> keyRoomsFirstPrefab;
    [SerializeField] private List<GameObject> keyRoomsSecondPrefab;
    [SerializeField] private List<GameObject> otherRoomsPrefab;
    [SerializeField] private List<GameObject> bung = new();
    [SerializeField] private GameObject prefabKey;
    [SerializeField] private GameObject prefabSheet;

    [Space, Header("Objects")] 
    [HideInInspector] public List<Node> allNodes = new();

    [Space, Header("Rooms")] 
    [HideInInspector] public SyncList<uint> famousPart = new();
    [HideInInspector] public SyncList<uint> monsterRoom = new();
    [HideInInspector] public SyncList<uint> keyRoomsFirst = new();
    [HideInInspector] public SyncList<uint> keyRoomsSecond = new();
    [HideInInspector] public SyncList<uint> otherRooms = new();
    [HideInInspector] public SyncList<uint> allRooms = new();

    //[HideInInspector] public SyncList<MainDoor> allMainDoors = new();
    [Space, Header("Items")] 
    [HideInInspector] public SyncList<uint> allKeys = new();
    [HideInInspector] public SyncList<uint> allSheets = new();
    [HideInInspector] public SyncList<uint> respawns = new();
    [HideInInspector] public SyncList<uint> ritePlaces = new();

    //
    [Space, Header("Params")]
    //params
    [SerializeField] private bool cyclePass;
    [SerializeField] private int maxFamousPart = 10;
    [SerializeField] private int currentFamousPart;
    [SerializeField] private int maxKeyRoomsFirst = 10;
    [SerializeField] private int currentKeyRoomsFirst;
    [SerializeField] private int maxKeyRoomsSecond = 10;
    [SerializeField] private int currentKeyRoomsSecond;
    [SerializeField] private int maxMonsterRooms = 1;
    [SerializeField] private int currentMonsterRooms;
    // [SerializeField] private int maxUniqueRooms = 1;
    // [SerializeField] private int currentUniqueRooms;
    [SerializeField] private int maxOtherRooms = 10;
    [SerializeField] private int currentOtherRooms;
    [SerializeField] private int countKeys = 5;
    [SerializeField] private int countSheets = 3;
    [SerializeField] private int cryToUnlockMonsterDoor = 3;

    public int currentAcceptedSheet;
    public int evacuatedPlayers;
    [SyncVar]
    public MonsterAI monster;

    [SerializeField] private GameObject nComp;
    [SerializeField] private GameObject comp;
    [SerializeField] private GameObject rot;

    public SpawnRoomManager respawn;

    [Space, Header("Сonditions")] public int minRequiredSheets = 1;

    [SyncVar] public int seed;
    private System.Random random;
    [SyncVar] public int cryCount;
    [SyncVar] public bool ready;

    private int tryCount;

    private NetworkManagerLobby managerLobby;
    
    public override void OnStartAuthority()
    {
        base.OnStartAuthority();
    }

    private void Awake()
    {
        managerLobby = NetworkManager.singleton as NetworkManagerLobby;
        managerLobby.location = this;
    }

    #region Server

    public override void OnStartServer()
    {
        //base.OnStartServer();
        countKeys = Math.Clamp(countKeys, 0, maxKeyRoomsFirst);
        managerLobby = NetworkManager.singleton as NetworkManagerLobby;
        managerLobby.location = this;
        NetworkManagerLobby.OnServerStopped += CleanUpServer;
        //NetworkManagerLobby.OnServerReadied += CheckToStartRound;
        CheckToStartRound();
    }

    private void Update()
    {
        //Debug.Log("update seed " + seed);
    }

    [ServerCallback]
    private void OnDestroy() => CleanUpServer();

    [Server]
    private void CleanUpServer()
    {
        NetworkManagerLobby.OnServerStopped -= CleanUpServer;
        //NetworkManagerLobby.OnServerReadied -= CheckToStartRound;
    }

    [ServerCallback]
    public void StartRound()
    {
        //RpcStartRound();
        Debug.Log("start Server");
    }

    [Server]
    private void CheckToStartRound()
    {
        //if (managerLobby.Players.Count(x => x.connectionToClient.isReady) != managerLobby.Players.Count) { return; }
        seed = Random.Range(int.MinValue, int.MaxValue);
        StartCoroutine(CheckSeed());
        //RpcStartCountdown();
    }

    #endregion

    #region Client

    [ClientRpc]
    private void RpcStartCountdown()
    {
        //Debug.Log(seed);
        //animator.enabled = true;
    }

    IEnumerator CheckSeed()
    {
        while (seed == 0)
            yield return null;

        // создание спавн комнаты
        //seed = -193808041; //для тестов
        //seed = -532597927; //для тестов
        seed = -371106178; //для тестов-371106178
        random = new System.Random(seed);

        var allNodesPass = new List<Node>();
        respawn = Instantiate(spawnRoomPrefab[random.Next(0, spawnRoomPrefab.Count)], Vector3.zero,
            Quaternion.LookRotation(-Vector3.forward)).GetComponent<SpawnRoomManager>();
        NetworkServer.Spawn(respawn.gameObject);
        allNodesPass.AddRange(respawn.nodesPass);
        allNodes.AddRange(respawn.nodes);

        // создание алтаря для выхода и сдачи
        var ritePlace = Instantiate(respawn.prefabRitePlace, respawn.positionRitePlace.position,
            respawn.positionRitePlace.rotation).GetComponent<RitePlace>();
        NetworkServer.Spawn(ritePlace.gameObject);
        ritePlace.locationGenerator = this;
        ritePlaces.Add(ritePlace.netId);
        var navMesh = respawn.GetComponentInChildren<UnityEngine.AI.NavMeshSurface>();

        //создание корридоров
        int iterationCount = 0;
        const int maxIterations = 100; // Лимит итераций для защиты от бесконечного цикла
        while (currentFamousPart < maxFamousPart && iterationCount < maxIterations)
        {
            iterationCount++;
            yield return NewRoom(allNodesPass, famousPartPrefab, ref currentFamousPart, true, famousPart);
        }

        iterationCount = 0;

        //соединение корридоров создавшихся рядом
        const float infelicity = 0.05f;
        if (cyclePass)
        {
            var tempNode = new List<Node>();
            foreach (var pass1 in allNodesPass)
            foreach (var pass2 in allNodesPass)
                if (pass1 != pass2)
                    if (Vector3.Distance(pass1.transform.position, pass2.transform.position) < infelicity)
                    {
                        if (!tempNode.Contains(pass1))
                            tempNode.Add(pass1);
                        if (!tempNode.Contains(pass2))
                            tempNode.Add(pass2);
                    }

            foreach (var node in tempNode)
                allNodesPass.Remove(node);
        }

        //создание уникальных комнат
        // while (currentUniqueRooms < maxUniqueRooms && tryCountRoom < 100)
        // {
        //     tryCountRoom++;
        //     yield return NewRoom(allNodes, uniqueRoomPrefab, ref currentUniqueRooms ,false,uniqueRoom);
        // }
        // tryCountRoom = 0;

        //создание комнаты монстра
        while (currentMonsterRooms < maxMonsterRooms && iterationCount < maxIterations)
        {
            iterationCount++;
            yield return NewRoom(allNodes, monsterRoomPrefab, ref currentMonsterRooms, false, monsterRoom);
            Debug.Log(iterationCount + " tryCountRoom");
        }

        iterationCount = 0;

        //todo: перенести данные монстра в LocationGenerator.cs
        //иногда стреляет ошибка из-за того что комната не нашла места для установки
        var bossRoomManager = NetworkClient.spawned[monsterRoom[0]].GetComponent<BossRoomManager>();
        var monsterGo = Instantiate(managerLobby.monsterPrefab,
            bossRoomManager.SpawnPoint.position, Quaternion.identity);
        NetworkServer.Spawn(monsterGo);
        monster = monsterGo.GetComponent<MonsterAI>();
        
        bossRoomManager.mainDoor.Locked = true;

        //создание комнат с ключами
        while (currentKeyRoomsFirst < maxKeyRoomsFirst && iterationCount < maxIterations)
        {
            iterationCount++;
            yield return NewRoom(allNodes, keyRoomsFirstPrefab, ref currentKeyRoomsFirst, false, keyRoomsFirst);
        }

        iterationCount = 0;

        //создание пустых комнат
        while (currentOtherRooms < maxOtherRooms && iterationCount < maxIterations)
        {
            iterationCount++;
            yield return NewRoom(allNodes, otherRoomsPrefab, ref currentOtherRooms, false, otherRooms);
        }

        iterationCount = 0;

        // Quaternion.LookRotation(-nodes[rndIndx1].transform.forward) *
        //     Quaternion.Inverse(roomNodes[rndIndx2].transform.rotation);
        //закрываем оставшиеся проходы
        foreach (var pass in allNodesPass)
        {
            var bungObj = Instantiate(bung[1], pass.transform.position,
                pass.transform.rotation);
            NetworkServer.Spawn(bungObj);
        }

        foreach (var pass in allNodes)
        {
            var bungObj = Instantiate(bung[0], pass.transform.position, pass.transform.rotation);
            NetworkServer.Spawn(bungObj);
        }

        //спавн ключей
        //todo: сделать спавн ключей в рандомных комнатах, а не по порядку
        //удалять занятые позиции для спавна предметов
        var allDoors = new List<MainDoor>();

        foreach (var id in allRooms)
        {
            var room = NetworkClient.spawned[id];
            if (room != null)
            {
                var roomManager = room.GetComponent<RoomManager>();
                if (roomManager.mainDoor != null)
                    allDoors.Add(roomManager.mainDoor);
            }
        }

        var lockedDoor = new List<MainDoor>();
        RoomManager oldRoom = null;
        int keyId = 0;
        

        while (allKeys.Count < countKeys)
        {
            if (++iterationCount > maxIterations)
            {
                Debug.LogError("Превышено максимальное количество попыток генерации ключей. Проверьте условия генерации.");
                break;
            }

            if (allDoors.Count == 0)
            {
                Debug.LogError("Список allDoors пуст. Невозможно продолжить генерацию ключей.");
                break;
            }

            var rndInxDoor = random.Next(0, allDoors.Count);
            var currentDoor = allDoors[rndInxDoor];
            bool keyGenerated = false;

            foreach (var id in keyRoomsFirst)
            {
                var currentRoom = NetworkClient.spawned[id];
                var roomManager = currentRoom.GetComponent<RoomManager>();

                // Проверяем условия: дверь не должна быть основной для комнаты, и это не старая комната
                if (roomManager.mainDoor != currentDoor && oldRoom != currentRoom)
                {
                    if (roomManager.currentPreset == null)
                    {
                        Debug.LogError($"В префабе комнаты {currentRoom.name} отсутствует пресет.");
                        continue;
                    }

                    var keySpawnPoints = roomManager.currentPreset.keySpawnPoint;

                    // Проверяем, что точки спавна ключей существуют
                    if (keySpawnPoints == null || keySpawnPoints.Count == 0)
                    {
                        Debug.LogError($"В префабе комнаты {currentRoom.name} отсутствуют точки для спавна ключей.");
                        continue;
                    }

                    // Устанавливаем параметры комнаты и двери
                    var randomRoom = new System.Random(roomManager.seed);
                    var randomInxPoint = randomRoom.Next(0, keySpawnPoints.Count);

                    var point = keySpawnPoints[randomInxPoint];

                    roomManager.lockedMainDoor = true;
                    roomManager.idKeyMainDoor = ++keyId;
                    lockedDoor.Add(currentDoor);

                    // Создаём ключ и устанавливаем параметры
                    var keyGo = Instantiate(prefabKey, point.transform.position, point.transform.rotation);
                    NetworkServer.Spawn(keyGo);

                    keyGo.GetComponent<DoorKey>().idKey = keyId;
                    allKeys.Add(keyGo.GetComponent<Key>().netId);
                    allDoors.Remove(currentDoor);
                    roomManager.currentPreset.keySpawnPoint.Remove(point);

                    oldRoom = roomManager;

                    keyGenerated = true; // Флаг, что ключ успешно сгенерирован
                    break; // Переходим к следующей двери
                }
            }

            if (!keyGenerated)
            {
                Debug.LogWarning("Не удалось сгенерировать ключ для текущей двери. Удаляем дверь из списка.");
                allDoors.Remove(currentDoor); // Удаляем дверь, чтобы избежать зацикливания
            }
        }

        //todo: распихать листы по всем запертым комнатам
        //todo: распихать листы по оставшимся комнатам
        //спавн листов 
        foreach (var door in lockedDoor)
        {
            if (allSheets.Count < countSheets)
            {
                if (door.roomManager.currentPreset != null)
                {
                    var randomRoom = new System.Random(door.roomManager.seed);
                    var keySpawnPoints = door.roomManager.currentPreset.keySpawnPoint;
                    var randomInxPoint = randomRoom.Next(0, keySpawnPoints.Count);
                    if (keySpawnPoints.Count > 0)
                    {
                        var point = keySpawnPoints[randomInxPoint];
                        var sheetGo = Instantiate(prefabSheet, point.transform.position, point.transform.rotation);
                        NetworkServer.Spawn(sheetGo);
                        allSheets.Add(sheetGo.GetComponent<Sheet>().netId);
                    }
                    else
                    {
                        var o = door.roomManager.gameObject;
                        Debug.LogError($"в комнате {o.name} keySpawnPoints == 0", o);
                    }
                }
                //lockedDoor.Remove(door);
            }
        }

        iterationCount = 0;

        while (allSheets.Count < countSheets)
        {
            if (++iterationCount > maxIterations)
            {
                Debug.LogError("Превышено максимальное количество попыток генерации бумажек. Проверьте условия генерации.");
                break;
            }

            if (allDoors.Count == 0)
            {
                Debug.LogError("Список allDoors пуст. Невозможно продолжить генерацию бумажек.");
                break;
            }

            var rndInxDoor = random.Next(0, allDoors.Count);
            var currentDoor = allDoors[rndInxDoor];
            var roomManager = currentDoor.roomManager;

            if (roomManager.currentPreset == null)
            {
                Debug.LogError($"В комнате {roomManager.gameObject.name} отсутствует пресет.");
                allDoors.Remove(currentDoor); // Удаляем дверь, чтобы избежать зацикливания
                continue;
            }

            var keySpawnPoints = roomManager.currentPreset.keySpawnPoint;

            // Проверяем, что точки спавна существуют
            if (keySpawnPoints == null || keySpawnPoints.Count == 0)
            {
                Debug.LogError($"В комнате {roomManager.gameObject.name} отсутствуют точки для спавна.");
                allDoors.Remove(currentDoor); // Удаляем дверь, чтобы избежать зацикливания
                continue;
            }

            var randomRoom = new System.Random(roomManager.seed);
            var randomInxPoint = randomRoom.Next(0, keySpawnPoints.Count);

            // Проверка индекса на корректность
            if (randomInxPoint < 0 || randomInxPoint >= keySpawnPoints.Count)
            {
                Debug.LogError($"Неверный индекс {randomInxPoint} для точек спавна в комнате {roomManager.gameObject.name}");
                allDoors.Remove(currentDoor); // Удаляем дверь, чтобы избежать зацикливания
                continue;
            }

            var point = keySpawnPoints[randomInxPoint];

            // Спавн бумажки
            var sheetGo = Instantiate(prefabSheet, point.transform.position, point.transform.rotation);
            NetworkServer.Spawn(sheetGo);
            allSheets.Add(sheetGo.GetComponent<Sheet>().netId);

            // Удаляем текущую дверь из списка, чтобы не использовать её снова
            allDoors.Remove(currentDoor);
        }

        ready = true;
        RpcCreateLocation();
        NetworkClient.localPlayer.gameObject.GetComponent<Player>().CmdReadySpawn();
        navMesh.BuildNavMesh();
    }

    #region Generation

    private object NewRoom(List<Node> nodes, List<GameObject> entitys, ref int currentEntitys, bool pass,
        SyncList<uint> currentRoomList)
    {
        int rndIndx1;
        var randomIndexPart = random.Next(0, entitys.Count);
        var goRoom = Instantiate(entitys[randomIndexPart], Vector3.zero, Quaternion.identity).transform;

        var roomManager = goRoom.GetComponent<RoomManager>();
        var roomNodes = pass ? roomManager.nodesPass : roomManager.nodes;
        var roomColliders = roomManager.GetComponents<BoxCollider>();
        int rndIndx2;

        while (tryCount < 100)
        {
            rndIndx2 = random.Next(0, roomNodes.Count);
            rndIndx1 = random.Next(0, nodes.Count);

            var roomRotation = Quaternion.LookRotation(-nodes[rndIndx1].transform.forward) *
                               Quaternion.Inverse(roomNodes[rndIndx2].transform.rotation);

            var roomPosition = nodes[rndIndx1].transform.position -
                               roomRotation *
                               roomNodes[rndIndx2].transform.position;

            var check = false;
            RaycastHit[] hits = new RaycastHit[roomColliders.Length];
            foreach (var roomCollider in roomColliders)
            {
                hits = Physics.BoxCastAll(roomPosition + roomRotation * roomCollider.center + Vector3.up * 5,
                roomCollider.bounds.extents, Vector3.down,
                roomRotation, 10f,
                1 << LayerMask.NameToLayer("Rooms"));
                
                foreach (var hit in hits)
                {
                    if (hit.collider != roomCollider)
                    {
                        check = true;
                        break;
                    }
                }
            }

            // ExtDebug.DrawBox(roomPosition + roomRotation * roomCollider.center + Vector3.up * 5,roomCollider.size / 2,roomRotation,Color.green);
            // Debug.Break();
            

            if (check)
            {
                tryCount++;
                if (tryCount >= 100)
                {
                    Destroy(goRoom.gameObject);
                    break;
                }

                continue;
            }

            goRoom.name += tryCount;
            goRoom.rotation = roomRotation;
            goRoom.position = roomPosition;
            var seedRoom = seed + allRooms.Count;

            NetworkServer.Spawn(goRoom.gameObject);
            roomNodes.Remove(roomNodes[rndIndx2]);
            nodes.Remove(nodes[rndIndx1]);
            nodes.AddRange(roomNodes);
            allNodes.AddRange(roomManager.nodes);
            ++currentEntitys;
            allRooms.Add(roomManager.netId);
            currentRoomList.Add(roomManager.netId);
            roomManager.location = this;
            roomManager.SetSeed(seedRoom);
            //roomManager.RpcGenerationRoom();
            //return new WaitForSeconds(0.5f);
            break;
        }

        tryCount = 0;
        //Instantiate(comp, nodes[rndIndx1].transform.position, Quaternion.identity, transform);
        return null;
    }

    #endregion

    [Server]
    public void UnlockMonsterRoom()
    {
        cryCount++;
        if (cryCount == cryToUnlockMonsterDoor)
            RpcUnlockMonsterRoom();
    }

    [ClientRpc]
    void RpcUnlockMonsterRoom()
    {
        var mainDoor = NetworkClient.spawned[monsterRoom[0]].GetComponent<RoomManager>().mainDoor;
        mainDoor.Locked = false;
        mainDoor.OpenDoor(mainDoor.transform.forward, 0);
    }

    [Server]
    public void EscapeRite()
    {
        RpcEscapeRite();
    }

    [ClientRpc]
    void RpcEscapeRite()
    {
        NetworkClient.spawned[ritePlaces[0]].GetComponent<RitePlace>().ChangeState();
    }

    [ClientRpc]
    public void RpcCreateLocation()
    {
        monster.location = this;
        managerLobby.CreateLocation();
    }

    #endregion
}