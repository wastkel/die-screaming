using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using Mirror.Examples.Basic;
using UnityEngine;

public class SoundManager : NetworkBehaviour
{
    private Coroutine updateVoiceAmplitude;
    [SyncVar] public float currentAmplitude;
    private float updateStep = 0.1f;
    private int mult = 2;
    private UIPlayer ui;
    
    public override void OnStartAuthority()
    {
        base.OnStartAuthority();
        enabled = true;
        ui = GetComponent<UIPlayer>();
        updateVoiceAmplitude = StartCoroutine(UpdateVoiceAmplitude());
    }

    private void Update()
    {
        ui.SoundVolumeScale.value = (int)Mathf.Lerp(ui.SoundVolumeScale.value, currentAmplitude,0.9f);
    }

    public void AddAmplitude(float sample)
    {
        if (sample > currentAmplitude)
            currentAmplitude = sample;
    }
    private IEnumerator UpdateVoiceAmplitude()
    {
        while (true)
        {
            yield return new WaitForSeconds(updateStep);
            currentAmplitude /= mult;
        }

        yield return null;
    }
}
