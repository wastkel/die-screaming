﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyManager : MonoBehaviour
{
    [SerializeField] GameObject users;
    [SerializeField] GameObject emptyUserPrefab;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public GameObject AddUserUIElement()
    {
        return Instantiate(emptyUserPrefab, users.transform);
    }

    public void RemoveUserUIElement(GameObject userUIElement)
    {
        Destroy(userUIElement);
    }
}
