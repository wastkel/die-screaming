using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IFsmNode<TKey> where TKey : struct
{
    void OnEnter(TKey from);
    void OnExit(TKey to);
    void Update(float dt);
}

public class BaseFsm<TKey, TNode> where TKey : struct where TNode : class, IFsmNode<TKey> 
{
    protected readonly Dictionary<TKey, TNode> Nodes = new Dictionary<TKey, TNode>();
    protected TNode SelectedNodeObj = null;
		
    public virtual TKey SelectedNode { get; protected set; }

    protected void SetNode(TKey key, TNode node)
    {
        if (node != null)
        {
            Nodes[key] = node;
        }
    }
		
    public void Start(TKey name)
    {
        if (SelectedNodeObj == null)
        {
            SwitchNode(name);
        }
    }
		
    public virtual void Force(TKey name)
    {
        SwitchNode(name);
    }

    public virtual void Update(float dt)
    {
    }

    protected virtual void SwitchNode(TKey name)
    {
        SelectedNode = name;
        Nodes.TryGetValue(name, out SelectedNodeObj);

        if (SelectedNodeObj == null)
        {
            Debug.LogError($"FSM[{GetType()}] switch to empty node [{name}]");
        }
    }
}
