﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System.Linq;
using System;
using System.IO;

[ExecuteAlways]
public class PrefabLightmapData : MonoBehaviour
{
    [Tooltip("Reassigns shaders when applying the baked lightmaps. Might conflict with some shaders like transparent HDRP.")]
    public bool releaseShaders = true;

    [System.Serializable]
    struct RendererInfo
    {
        public Renderer renderer;
        public int lightmapIndex;
        public Vector4 lightmapOffsetScale;
    }
    [System.Serializable]
    struct LightInfo
    {
        public Light light;
        public int lightmapBaketype;
        public int mixedLightingMode;
    }

    [SerializeField]
    Presets[] presetsInfo;

    [System.Serializable]
    struct Presets
    {
        [SerializeField]
        public RendererInfo[] m_RendererInfo;
        [SerializeField]
        public Texture2D[] m_Lightmaps;
        [SerializeField]
        public Texture2D[] m_LightmapsDir;
        [SerializeField]
        public Texture2D[] m_ShadowMasks;
        [SerializeField]
        public LightInfo[] m_LightInfo;
    }

    public void Init(int indexPreset)
    {
        if (presetsInfo[indexPreset].m_RendererInfo == null || presetsInfo[indexPreset].m_RendererInfo.Length == 0)
            return;

        var lightmaps = LightmapSettings.lightmaps;
        int[] offsetsindexes = new int[presetsInfo[indexPreset].m_Lightmaps.Length];
        int counttotal = lightmaps.Length;
        List<LightmapData> combinedLightmaps = new List<LightmapData>();

        for (int i = 0; i < presetsInfo[indexPreset].m_Lightmaps.Length; i++)
        {
            bool exists = false;
            for (int j = 0; j < lightmaps.Length; j++)
            {

                if (presetsInfo[indexPreset].m_Lightmaps[i] == lightmaps[j].lightmapColor)
                {
                    exists = true;
                    offsetsindexes[i] = j;

                }

            }
            if (!exists)
            {
                offsetsindexes[i] = counttotal;
                var newlightmapdata = new LightmapData
                {
                    lightmapColor = presetsInfo[indexPreset].m_Lightmaps[i],
                    lightmapDir = presetsInfo[indexPreset].m_LightmapsDir.Length == presetsInfo[indexPreset].m_Lightmaps.Length ? presetsInfo[indexPreset].m_LightmapsDir[i] : default(Texture2D),
                    shadowMask = presetsInfo[indexPreset].m_ShadowMasks.Length == presetsInfo[indexPreset].m_Lightmaps.Length ? presetsInfo[indexPreset].m_ShadowMasks[i] : default(Texture2D),
                };

                combinedLightmaps.Add(newlightmapdata);

                counttotal += 1;


            }

        }

        var combinedLightmaps2 = new LightmapData[counttotal];

        lightmaps.CopyTo(combinedLightmaps2, 0);
        combinedLightmaps.ToArray().CopyTo(combinedLightmaps2, lightmaps.Length);

        bool directional = true;

        foreach (Texture2D t in presetsInfo[indexPreset].m_LightmapsDir)
        {
            if (t == null)
            {
                directional = false;
                break;
            }
        }

        LightmapSettings.lightmapsMode = (presetsInfo[indexPreset].m_LightmapsDir.Length == presetsInfo[indexPreset].m_Lightmaps.Length && directional) ? LightmapsMode.CombinedDirectional : LightmapsMode.NonDirectional;
        ApplyRendererInfo(presetsInfo[indexPreset].m_RendererInfo, offsetsindexes, presetsInfo[indexPreset].m_LightInfo);
        LightmapSettings.lightmaps = combinedLightmaps2;
    }

    //void OnEnable()
    //{

    //    SceneManager.sceneLoaded += OnSceneLoaded;

    //}

    //// called second
    ////void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    ////{
    ////    Init(1);
    ////}

    //// called when the game is terminated
    //void OnDisable()
    //{
    //    SceneManager.sceneLoaded -= OnSceneLoaded;
    //}



    void ApplyRendererInfo(RendererInfo[] infos, int[] lightmapOffsetIndex, LightInfo[] lightsInfo)
    {
        for (int i = 0; i < infos.Length; i++)
        {
            var info = infos[i];

            info.renderer.lightmapIndex = lightmapOffsetIndex[info.lightmapIndex];
            info.renderer.lightmapScaleOffset = info.lightmapOffsetScale;

            if (releaseShaders)
            {
                // You have to release shaders.
                Material[] mat = info.renderer.sharedMaterials;
                for (int j = 0; j < mat.Length; j++)
                {
                    if (mat[j] != null && Shader.Find(mat[j].shader.name) != null)
                    {
                        mat[j].shader = Shader.Find(mat[j].shader.name);
                    }

                }
            }

        }

        for (int i = 0; i < lightsInfo.Length; i++)
        {
            LightBakingOutput bakingOutput = new LightBakingOutput();
            bakingOutput.isBaked = true;
            //bakingOutput.lightmapBakeType = (LightmapBakeType)lightsInfo[i].lightmapBaketype;
            //bakingOutput.mixedLightingMode = (MixedLightingMode)lightsInfo[i].mixedLightingMode;
            bakingOutput.lightmapBakeType = LightmapBakeType.Mixed;
            bakingOutput.mixedLightingMode = MixedLightingMode.IndirectOnly;

            lightsInfo[i].light.bakingOutput = bakingOutput;

        }


    }

#if UNITY_EDITOR
    [UnityEditor.MenuItem("Tools/Bake Prefab Lightmaps")]
    static void GenerateLightmapInfo()
    {
        if (UnityEditor.Lightmapping.giWorkflowMode != UnityEditor.Lightmapping.GIWorkflowMode.OnDemand)
        {
            Debug.LogError("ExtractLightmapData requires that you have baked you lightmaps and Auto mode is disabled.");
            return;
        }
        

        PrefabLightmapData[] prefabs = FindObjectsOfType<PrefabLightmapData>();

        foreach (var instance in prefabs)
        {
            instance.gameObject.SetActive(true);
            var gameObject = instance.gameObject;
            var presets = instance.GetComponentsInChildren<RoomPreset>(true);
            instance.presetsInfo = new Presets[presets.Length];
            string roomName = instance.name;
            string roomFolder = Path.Combine("Assets/Lightmaps", roomName);

            for (int i = 0;i < presets.Length; i++)
            {
                CreateDirectoryIfNotExists(roomFolder);
                string presetFolder = Path.Combine(roomFolder, presets[i].name);

                if (AssetDatabase.IsValidFolder(presetFolder))
                    AssetDatabase.DeleteAsset(presetFolder);
                Directory.CreateDirectory(presetFolder);

                presets[i].gameObject.SetActive(true);
                EditorUtility.DisplayProgressBar("Processing", $"Bake {roomName} / Preset {presets[i].name}", 0);
                UnityEditor.Lightmapping.Bake();
                var rendererInfos = new List<RendererInfo>();
                var lightmaps = new List<Texture2D>();
                var lightmapsDir = new List<Texture2D>();
                var shadowMasks = new List<Texture2D>();
                var lightsInfos = new List<LightInfo>();
                
                GenerateLightmapInfo(gameObject, rendererInfos, lightmaps, lightmapsDir, shadowMasks, lightsInfos);

                var tmpLightmaps = new List<Texture2D>();
                var tmpLightmapDir = new List<Texture2D>();
                var tmpShadowMask = new List<Texture2D>();

                for (int j = 0; j < lightmaps.Count; j++)
                {
                    string colorPath = Path.Combine(presetFolder, $"{presets[i].name}_LightmapColor_{j}.png");
                    Texture2D lightmap = lightmaps[j];
                    tmpLightmaps.Add(SaveTextureAsPNG(lightmap, colorPath));
                }

                for (int j = 0; j < lightmapsDir.Count; j++)
                {
                    string dirPath = Path.Combine(presetFolder, $"{presets[i].name}_LightmapDir_{j}.png");
                    Texture2D lightmapDir = lightmapsDir[j];
                    tmpLightmapDir.Add(SaveTextureAsPNG(lightmapDir, dirPath));
                }

                for (int j = 0; j < shadowMasks.Count; j++)
                {
                    string maskPath = Path.Combine(presetFolder, $"{presets[i].name}_ShadowMask_{j}.png");
                    Texture2D shadowMask = shadowMasks[j];
                    tmpShadowMask.Add(SaveTextureAsPNG(shadowMask, maskPath));
                }

                instance.presetsInfo[i].m_RendererInfo = rendererInfos.ToArray();
                instance.presetsInfo[i].m_LightInfo = lightsInfos.ToArray();
                instance.presetsInfo[i].m_Lightmaps = tmpLightmaps.ToArray();
                instance.presetsInfo[i].m_LightmapsDir = tmpLightmapDir.ToArray();
                instance.presetsInfo[i].m_ShadowMasks = tmpShadowMask.ToArray();
#if UNITY_2018_3_OR_NEWER
                var targetPrefab = PrefabUtility.GetCorrespondingObjectFromOriginalSource(instance.gameObject) as GameObject;
                if (targetPrefab != null)
                {
                    GameObject root = PrefabUtility.GetOutermostPrefabInstanceRoot(instance.gameObject);// 根结点
                                                                                                        //如果当前预制体是是某个嵌套预制体的一部分（IsPartOfPrefabInstance）
                    if (root != null)
                    {
                        GameObject rootPrefab = PrefabUtility.GetCorrespondingObjectFromSource(instance.gameObject);
                        string rootPath = AssetDatabase.GetAssetPath(rootPrefab);
                        //打开根部预制体
                        PrefabUtility.UnpackPrefabInstanceAndReturnNewOutermostRoots(root, PrefabUnpackMode.OutermostRoot);
                        try
                        {
                            //Apply各个子预制体的改变
                            PrefabUtility.ApplyPrefabInstance(instance.gameObject, InteractionMode.AutomatedAction);
                        }
                        catch { }
                        finally
                        {
                            //重新更新根预制体
                            PrefabUtility.SaveAsPrefabAssetAndConnect(root, rootPath, InteractionMode.AutomatedAction);
                        }
                    }
                    else
                    {
                        PrefabUtility.ApplyPrefabInstance(instance.gameObject, InteractionMode.AutomatedAction);
                    }
                }
#else
                var targetPrefab = UnityEditor.PrefabUtility.GetPrefabParent(gameObject) as GameObject;
                if (targetPrefab != null)
                {
                    //UnityEditor.Prefab
                    UnityEditor.PrefabUtility.ReplacePrefab(gameObject, targetPrefab);
                }
#endif
                presets[i].gameObject.SetActive(false);
            }
            instance.gameObject.SetActive(false);
        }
        EditorUtility.ClearProgressBar();
    }

    static void GenerateLightmapInfo(GameObject root, List<RendererInfo> rendererInfos, List<Texture2D> lightmaps, List<Texture2D> lightmapsDir, List<Texture2D> shadowMasks, List<LightInfo> lightsInfo)
    {
        var renderers = root.GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer renderer in renderers)
        {
            if (renderer.lightmapIndex != -1)
            {
                RendererInfo info = new RendererInfo();
                info.renderer = renderer;

                if (renderer.lightmapScaleOffset != Vector4.zero)
                {
                    //1ibrium's pointed out this issue : https://docs.unity3d.com/ScriptReference/Renderer-lightmapIndex.html
                    if (renderer.lightmapIndex < 0 || renderer.lightmapIndex == 0xFFFE) continue;
                    info.lightmapOffsetScale = renderer.lightmapScaleOffset;

                    Texture2D lightmap = LightmapSettings.lightmaps[renderer.lightmapIndex].lightmapColor;
                    Texture2D lightmapDir = LightmapSettings.lightmaps[renderer.lightmapIndex].lightmapDir;
                    Texture2D shadowMask = LightmapSettings.lightmaps[renderer.lightmapIndex].shadowMask;

                    info.lightmapIndex = lightmaps.IndexOf(lightmap);
                    if (info.lightmapIndex == -1)
                    {
                        info.lightmapIndex = lightmaps.Count;
                        lightmaps.Add(lightmap);
                        lightmapsDir.Add(lightmapDir);
                        shadowMasks.Add(shadowMask);
                    }

                    rendererInfos.Add(info);
                }

            }
        }

        var lights = root.GetComponentsInChildren<Light>(true);

        foreach (Light l in lights)
        {
            LightInfo lightInfo = new LightInfo();
            lightInfo.light = l;
            lightInfo.lightmapBaketype = (int)l.lightmapBakeType;
#if UNITY_2020_1_OR_NEWER
            lightInfo.mixedLightingMode = (int)UnityEditor.Lightmapping.lightingSettings.mixedBakeMode;
#elif UNITY_2018_1_OR_NEWER
            lightInfo.mixedLightingMode = (int)UnityEditor.LightmapEditorSettings.mixedBakeMode;
#else
            lightInfo.mixedLightingMode = (int)l.bakingOutput.lightmapBakeType;            
#endif
            lightsInfo.Add(lightInfo);

        }
    }
#endif

    private static Texture2D SaveTextureAsPNG(Texture2D texture, string path)
    {
        if (texture == null)
            return null;

        // Получаем путь к текстуре в проекте
        string assetPath = AssetDatabase.GetAssetPath(texture);
        TextureImporter importer = AssetImporter.GetAtPath(assetPath) as TextureImporter;

        if (importer != null)
        {
            // Если текстура не доступна для чтения, сделаем её таковой
            if (!importer.isReadable)
            {
                importer.isReadable = true;
                importer.SaveAndReimport(); // Сохраняем и перезагружаем текстуру
            }
        }

        // Если текстура сжата, создаем копию в несжатом формате
        Texture2D textureToSave = new Texture2D(texture.width, texture.height, TextureFormat.ARGB32, false);

        // Копируем пиксели из сжатой текстуры
        textureToSave.SetPixels(0, 0, texture.width, texture.height, texture.GetPixels());
        textureToSave.Apply();

        // Сохраняем текстуру в PNG
        byte[] bytes = textureToSave.EncodeToPNG();
        File.WriteAllBytes(path, bytes);

        // Импортируем текстуру и получаем её импортер
        AssetDatabase.ImportAsset(path);
        TextureImporter textureImporter = AssetImporter.GetAtPath(path) as TextureImporter;

        if (textureImporter != null)
        {
            // Устанавливаем тип текстуры как Lightmap
            textureImporter.textureType = TextureImporterType.Lightmap;
            textureImporter.SaveAndReimport(); // Применяем изменения
        }

        return AssetDatabase.LoadAssetAtPath<Texture2D>(path);
    }

    private static void CreateDirectoryIfNotExists(string path)
    {
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
            Debug.Log($"Created directory: {path}");
        }
    }

}