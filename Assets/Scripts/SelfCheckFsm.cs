using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISelfCheckFsmNode<TKey> : IFsmNode<TKey> where TKey : struct
{
    TKey? CheckTransition();
}

public abstract class SelfCheckFsm<TKey, TNode> : BaseFsm<TKey, TNode> where TKey : struct where TNode : class, ISelfCheckFsmNode<TKey>
{
    protected abstract bool Equals(TKey key1, TKey key2);

    public override void Update(float dt)
    {
        base.Update(dt);

        SwitchNode(SelectedNode);

        var targetKey = SelectedNodeObj?.CheckTransition();
        if (targetKey.HasValue)
        {
            var key = targetKey.Value;
            var prevKey = SelectedNode;

            if (!Equals(key, prevKey))
            {
                SelectedNodeObj?.OnExit(key);
                SwitchNode(key);
                SelectedNodeObj?.OnEnter(prevKey);
            }
        }
        SelectedNodeObj?.Update(dt);
    }
}
