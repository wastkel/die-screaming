using UnityEngine;

namespace Mirror.Examples.Pong
{
    public class PlayerControler : NetworkBehaviour
    {
        public float speed = 30;
        public Rigidbody rigidbody;
        [SerializeField] Camera camera;

        void FixedUpdate()
        {
            if (isLocalPlayer)
                CmdMove();
        }

        [Command]
        void CmdMove()
        {
            RpcMove();
        }

        [ClientRpc]
        void RpcMove()
        {
            rigidbody.velocity = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")) * speed * Time.fixedDeltaTime;
        }
    }
}
