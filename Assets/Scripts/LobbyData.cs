using Mirror;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class LobbyData : NetworkBehaviour
{
    public static LobbyData instance;
    private List<Player> players = new ();
    public List<Player> Players 
    {
        get
        {
            if (players == null)
                players = new List<Player>();
            else
                players.Clear();

            foreach (var id in playerIds)
                if (NetworkClient.spawned.TryGetValue(id, out NetworkIdentity networkIdentity))
                    players.Add(networkIdentity.GetComponent<Player>());
            return players;
        }
    }
    public SyncList<uint> playerIds = new ();

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);
    }

    public override void OnStartClient()
    {
        base.OnStartClient();
        playerIds.Callback += OnPlayerIdsChanged;
    }
    
    public override void OnStopClient()
    {
        playerIds.Callback -= OnPlayerIdsChanged;
        base.OnStopClient();
    }

    [Server]
    public void AddPlayer(NetworkIdentity player)
    {
        if (!playerIds.Contains(player.netId))
            playerIds.Add(player.netId);
    }
    
    [Server]
    public void RemovePlayer(NetworkIdentity player)
    {
        if (playerIds.Contains(player.netId))
            playerIds.Remove(player.netId);
    }

    private void OnPlayerIdsChanged(SyncList<uint>.Operation op, int index, uint oldItem, uint newItem)
    {
        
    }
}