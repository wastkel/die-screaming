using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnPlaceholder : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(transform.position + transform.forward, new Vector3(1, 2, 1));
    }
}
