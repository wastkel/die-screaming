﻿using System;
using System.Collections;
using System.Collections.Generic;
using MathD;
using UnityEngine;
using Mirror;
using Unity.Mathematics;
using UnityEditor;
using UnityEngine.AI;
using UnityEngine.Serialization;

public enum MonsterStates
{
    None,
    Idle,
    Search,
    Attack,
    Grab,
    RunningTowardsNoise
}

public class MonsterFsmNode : ISelfCheckFsmNode<MonsterStates>
{
    public MonsterAI Monster;

    public virtual void OnEnter(MonsterStates from)
    {
    }

    public virtual void OnExit(MonsterStates to)
    {
    }

    public virtual void Update(float dt)
    {
        UpdateState(dt);
        foreach (var id in Monster.location.allRooms)
        {
            var room = NetworkClient.spawned[id].GetComponent<RoomManager>();
            if (room != null)
            {
                if (room.mainDoor != null)
                    DoorTryOpen(room.mainDoor);
            }
        }
        
    }

    public virtual void Init()
    {
    }

    public virtual MonsterStates? CheckTransition()
    {
        return null;
    }

    protected virtual void UpdateState(float dt)
    {
    }
    
    void DoorTryOpen(Door door)
    {
        if (!door.open)
        {
            var position = Monster.transform.position;
            var direction = position - door.transform.position.SetY(position.y);
            var sqLength = MathD.Math.SqLength(direction);
            if (!(sqLength > door.interactionRange * door.interactionRange))
            {
                var corners = Monster.agent.path.corners;
                for (var i = 0; i < corners.Length - 1; i++)
                {
                    if (i == 3) //TODO: constant limit iterations
                        break;
                    var ray = new Ray(corners[i] + Vector3.up * 0.5f,
                        (corners[i + 1] + Vector3.up * 0.5f) - (corners[i] + Vector3.up * 0.5f));
                    if (Physics.Raycast(ray, out var hit))
                        if (door == hit.transform.GetComponentInParent<MainDoor>())
                        {
                            door.OpenDoor(Monster.transform.position);
                            break;
                        }
                }
            }
        }
    }
}

public class MonsterFsm : SelfCheckFsm<MonsterStates, MonsterFsmNode>
{
    public MonsterStates CurrentNode
    {
        get => SelectedNode;
        set => SelectedNode = value;
    }
    
    public float StateFinishTime;

    public List<RoomManager> PatrolRoom = new ();

    private int currentPoint;
    public int CurrentPoint
    {
        get => currentPoint;
        set
        {
            currentPoint = value;
            if (value >= PatrolRoom.Count)
                currentPoint = 0;
        }
    }
    
    public MonsterFsm(MonsterAI monster)
    {
        foreach (var id in monster.location.keyRoomsFirst)
            PatrolRoom.Add(NetworkClient.spawned[id].GetComponent<RoomManager>());
        
        SetNode(MonsterStates.Idle, new IdleNode { Monster = monster });
        SetNode(MonsterStates.Search, new SearchNode() { Monster = monster });
        SetNode(MonsterStates.Attack, new AttackNode() { Monster = monster });
        SetNode(MonsterStates.Grab, new GrabNode() { Monster = monster });
        SetNode(MonsterStates.RunningTowardsNoise, new RunningTowardsNoiseNode() { Monster = monster });
        
        foreach (var node in Nodes)
            node.Value.Init();

        Start(MonsterStates.Idle);
    }
    
    protected override bool Equals(MonsterStates key1, MonsterStates key2)
    {
        return key1 == key2;
    }
}

public class IdleNode : MonsterFsmNode
{
    private Vector3 patrolPoint;
    
    public override void OnEnter(MonsterStates from)
    {
        var currentRoom = Monster.Fsm.PatrolRoom[Monster.Fsm.CurrentPoint];
        patrolPoint = currentRoom.mainDoor.Locked
            ? currentRoom.mainDoor.transform.position
            : currentRoom.transform.position;
        
        Monster.agent.SetDestination(patrolPoint);
        
        base.OnEnter(from);
    }
    
    public override MonsterStates? CheckTransition()
    {
        if (Monster.IsDetectedPlayer)
            return MonsterStates.Attack;
        if (!Monster.pointInterestVerified)
            return MonsterStates.RunningTowardsNoise;
        return null;
    }

    protected override void UpdateState(float dt)
    {
        base.UpdateState(dt);
        var agent = Monster.agent;
        if (!agent.pathPending && agent.remainingDistance <= agent.stoppingDistance)
        {
            //todo: remainingDistance всегда меньше 1
            Monster.Fsm.CurrentPoint++;
            var currentRoom = Monster.Fsm.PatrolRoom[Monster.Fsm.CurrentPoint];
            // patrolPoint = currentRoom.mainDoor.Locked
            //     ? currentRoom.mainDoor.transform.position
            //     : currentRoom.transform.position;
            patrolPoint = currentRoom.mainDoor.transform.position;
            agent.SetDestination(patrolPoint);
        }
    }
}

public class SearchNode : MonsterFsmNode
{
    private float searchTime;
    public override void OnEnter(MonsterStates from)
    {
        base.OnEnter(from);
        Monster.target = null;
        searchTime = Monster.searchTime + Time.time;
    }

    protected override void UpdateState(float dt)
    {
        base.UpdateState(dt);
    }

    public override MonsterStates? CheckTransition()
    {
        if (Monster.IsDetectedPlayer)
            return MonsterStates.Attack;
        if (!Monster.pointInterestVerified)
            return MonsterStates.RunningTowardsNoise;
        if (Time.time >= searchTime )
            return MonsterStates.Idle;
        return null;
    }
}

public class AttackNode : MonsterFsmNode
{
    public override void OnEnter(MonsterStates from)
    {
        base.OnEnter(from);
        Monster.agent.SetDestination(Monster.target.transform.position);
    }

    protected override void UpdateState(float dt)
    {
        base.UpdateState(dt);
        var agent = Monster.agent;
        if (agent.remainingDistance >= agent.stoppingDistance)
            agent.SetDestination(Monster.target.transform.position);
    }
    
    public override MonsterStates? CheckTransition()
    {
        if (!Monster.IsDetectedPlayer)
            return MonsterStates.Search;
        var distance = Monster.transform.position - Monster.target.transform.position;
        if (Monster.grabDistance * Monster.grabDistance >= MathD.Math.SqLength(distance))
            return MonsterStates.Grab;
        return null;
    }
    
    public override void OnExit(MonsterStates from)
    {
        base.OnEnter(from);
    }
}

public class RunningTowardsNoiseNode : MonsterFsmNode
{
    public override void OnEnter(MonsterStates from)
    {
        base.OnEnter(from);
        Monster.agent.SetDestination(Monster.lastInterestingPoints);
    }

    protected override void UpdateState(float dt)
    {
        base.UpdateState(dt);
        if (!Monster.pointInterestVerified)
        {
            var agent = Monster.agent;
            agent.SetDestination(Monster.lastInterestingPoints);
            if (!agent.pathPending && agent.remainingDistance <= agent.stoppingDistance)
                Monster.pointInterestVerified = true;
        }
    }
    
    public override MonsterStates? CheckTransition()
    {
        if (Monster.IsDetectedPlayer)
            return MonsterStates.Attack;
        if (Monster.pointInterestVerified)
            return MonsterStates.Idle;
        return null;
    }
    
    public override void OnExit(MonsterStates from)
    {
        base.OnEnter(from);
    }
}

public class GrabNode : MonsterFsmNode
{
    private float grabTime;
    public override void OnEnter(MonsterStates from)
    {
        base.OnEnter(from);
        Monster.KillPlayer(Monster.target.connectionToClient);
        grabTime = 0 + Time.time;
    }

    protected override void UpdateState(float dt)
    {
        base.UpdateState(dt);
    }

    public override MonsterStates? CheckTransition()
    {
        if (Time.time >= grabTime)
            return MonsterStates.Idle;
        return null;
    }
    
    public override void OnExit(MonsterStates from)
    {
        base.OnEnter(from);
        Monster.target = null;
    }
}

public class MonsterAI : NetworkBehaviour
{
    public LocationGenerator location;
    Animator animator;
    public NavMeshAgent agent;
    private Interaction interactionObj;
    [SerializeField] private LayerMask layerMask;
    
    public NetworkManagerLobby managerLobby;
    public MonsterFsm Fsm;
    public float searchTime = 10f;
    public bool IsDetectedPlayer => SearchPlayer();
    public float visionDistance = 5f;
    public float interactionRad = 0.785f;
    public float grabDistance = 2f;
    public Player target;
    public bool pointInterestVerified;
    public Vector3 lastInterestingPoints;

    private void Awake()
    {
        NetworkManagerLobby.OnCreateLocation += CreateFsm;
        animator = GetComponent<Animator>();
        managerLobby = NetworkManager.singleton as NetworkManagerLobby;
        agent = GetComponent<NavMeshAgent>();
    }

    public override void OnStartServer()
    {
        base.OnStartServer();
        enabled = true;
    }

    private void Update()
    {
        if (Fsm == null)
            return;
        Fsm.Update(Time.deltaTime);
        animator.SetFloat("Speed", math.abs(agent.velocity.magnitude));
    }

    void CreateFsm(NetworkConnection conn, bool all)
    {
        Fsm = new MonsterFsm(this);
    }
    private bool SearchPlayer()
    {
        var heightOffsetVector = Vector3.up * 0.5f; //Времянка по росту
        foreach (var player in managerLobby.LobbyData.Players)
        {
            if (player.isDead) continue;
            var direction = player.transform.position - transform.position + heightOffsetVector;
            var sqLength = MathD.Math.SqLength(direction);
            if (!(sqLength > visionDistance * visionDistance))
            {
                var cosine = Vector3.Dot(direction.normalized, transform.forward);
                if (cosine > interactionRad)
                {
                    var ray = new Ray(transform.position + heightOffsetVector, direction);
                    if (Physics.Raycast(ray, out var hit) && hit.transform == player.transform)
                    {
                        target = player;
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public void AddPointInteresting(Vector3 pos)
    {
        var lastSqLength = MathD.Math.SqLength(transform.position - lastInterestingPoints);
        var newSqLength = MathD.Math.SqLength(transform.position - pos);
        if (newSqLength < lastSqLength || pointInterestVerified)
        {
            pointInterestVerified = false;
            lastInterestingPoints = pos;
        }
        
    }
    
    void OpenDoor(MainDoor door)
    {
        RpcOpenDoor(transform.position, door.roomManager);
    }

    [ClientRpc]
    void RpcOpenDoor(Vector3 enemyPos, RoomManager roomIdentity)
    {
        roomIdentity.mainDoor.OpenDoor(enemyPos);
    }
    
    private void OnTriggerEnter(Collider other)
    {
        
    }
    
    [Server]
    public void KillPlayer(NetworkConnectionToClient conn)
    {
        var targetIdentity = conn.identity;
        targetIdentity.GetComponent<Player>().isDead = true;
        var rnd = new System.Random();
        var respId = location.respawns[rnd.Next(0, location.respawns.Count)];
        var resp = NetworkClient.spawned[respId].GetComponent<RespawnSheet>();
        resp.currentPlayer = targetIdentity;
        resp.gameObject.SetActive(true);
        TargetKillPlayer(conn, respId);
        RpcKillPlayer(targetIdentity);
    }

    [TargetRpc]
    private void TargetKillPlayer(NetworkConnectionToClient conn, uint respId)
    {
        var resp = NetworkClient.spawned[respId].GetComponent<RespawnSheet>();
        var player = resp.currentPlayer.GetComponent<Player>();
        player.transform.position = resp.transform.position - resp.transform.rotation * new Vector3(0, 1.5f, -0.246434f);
        player.characterController.enabled = false;
        player.movementController.enabled = false;
        player.interactionController.enabled = false;
        player.inventory.enabled = false;
    }

    [ClientRpc]
    public void RpcKillPlayer(NetworkIdentity identity)
    {
        if (!identity.isLocalPlayer)
            identity.gameObject.SetActive(false);
        
        var alivePlayers = 0;
        foreach (var playerTmp in managerLobby.LobbyData.Players)
            if (!playerTmp.isDead)
                ++alivePlayers;
        if (alivePlayers == 0)
            Debug.Log("GameOver");
    }
    
    public void MakeSound(Vector3 center, float distance)
    {
        var SqDistance = MathD.Math.SqLength(transform.position - center);
        if (SqDistance < distance * distance)
            AddPointInteresting(center);
    }
    
    private void OnDrawGizmos()
    {
        var oldCorner = transform.position;
        foreach (var corner in agent.path.corners)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawLine(oldCorner,corner);
            oldCorner = corner;
        }
    }
}
