﻿using UnityEditor;
using UnityEngine;
using UnityEditor.SceneManagement;

[CustomEditor(typeof(RoomManager))]
public class RoomManagerEditor : Editor
{
    private RoomManager roomManager;

    private void OnEnable()
    {
        roomManager = (RoomManager)target;
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Проверка комнаты", EditorStyles.boldLabel);

        bool hasNodes = roomManager.nodes != null && roomManager.nodes.Count > 0;
        DrawCheckBox("Наличие узлов", hasNodes, () =>
        {
            var nodeHolder = new GameObject("Nodes");
            nodeHolder.transform.parent = roomManager.transform;
            new GameObject("Node").AddComponent<Node>().transform.parent = nodeHolder.transform;
        }, "Add Node");

        bool hasNodesPath = roomManager.nodesPass != null && roomManager.nodesPass.Count > 0;
        DrawCheckBox("Наличие узлов коридоров", hasNodesPath, () =>
        {
            var nodeHolder = new GameObject("Nodes");
            nodeHolder.transform.parent = roomManager.transform;
            new GameObject("Node").AddComponent<Node>().transform.parent = nodeHolder.transform;
        }, "Add path");

        DrawCheckBox("Наличие главной двери", roomManager.mainDoor != null, null, "Add path");

        bool layer = LayerMask.NameToLayer("Rooms") == roomManager.gameObject.layer;
        DrawCheckBox("Слой", layer, () =>
        {
            roomManager.gameObject.layer = LayerMask.NameToLayer("Rooms");
        }, "Fix layer");

        bool hasCollider = roomManager.gameObject.GetComponent<BoxCollider>() != null;
        DrawCheckBox("Наличие коллайдера", hasCollider, () =>
        {
            roomManager.gameObject.AddComponent<BoxCollider>();
        }, "Add collider");

        bool hasPresets = roomManager.transform.Find("Presets");
        DrawCheckBox("Наличие пресетов", hasPresets, () =>
        {
            var presetsHolder = new GameObject("Presets");
            presetsHolder.transform.parent = roomManager.transform;
            new GameObject("new").AddComponent<RoomPreset>().transform.parent = presetsHolder.transform;
        }, "Add Preset");

        EditorGUILayout.Space();

        if (GUI.changed)
        {
            EditorUtility.SetDirty(roomManager);
        }
    }

    private void DrawCheckBox(string label, bool condition, System.Action fixAction, string fixButtonLabel)
    {
        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.LabelField(label, GUILayout.Width(200));
        GUIStyle style = new GUIStyle(EditorStyles.label);

        if (condition)
        {
            style.normal.textColor = Color.green;
            EditorGUILayout.LabelField("✓", style, GUILayout.Width(20));
        }
        else
        {
            style.normal.textColor = Color.red;
            EditorGUILayout.LabelField("✗", style, GUILayout.Width(20));
        }

        if (!condition && fixAction != null)
        {
            if (GUILayout.Button(fixButtonLabel, GUILayout.Width(100)))
            {
                if (PrefabStageUtility.GetCurrentPrefabStage() != null)
                    fixAction.Invoke();
                else
                    Debug.LogError("Для изменения нужно открыть редактор префабов");
            }
        }

        EditorGUILayout.EndHorizontal();
    }
}