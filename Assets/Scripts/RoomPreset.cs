using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RoomPreset : MonoBehaviour
{
    public List<SpawnPointItem> keySpawnPoint;
    public List<Transform> itemSpawnPoint;
    public RoomManager room;
    public List<Door> containerDoors;
    public List<Transform> respawnPlaceHolders;

    private void OnEnable()
    {
        room = GetComponentInParent<RoomManager>();
        keySpawnPoint = GetComponentsInChildren<SpawnPointItem>().ToList();
        containerDoors = GetComponentsInChildren<Door>().ToList();
    }
}