using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LampAnimation : MonoBehaviour
{
    [SerializeField] private AnimationCurve[] curves;
    [SerializeField] private int currentCurve = 0;
    private Light lightSource;
    private float defaultValue;
    private float timer;
    public float delay = 30f;
    private bool isPlaying;
    private bool isWaiting;

    private void Start()
    {
        lightSource = GetComponentInChildren<Light>();
        defaultValue = lightSource.intensity;
        PlayAnimation();
    }

    private void Update()
    {
        if (isPlaying)
        {
            timer += Time.deltaTime;
            lightSource.intensity = defaultValue * curves[currentCurve].Evaluate(timer);

            if (timer >= curves[currentCurve].keys[curves[currentCurve].length - 1].time)
            {
                isPlaying = false;
                isWaiting = true;
                timer = 0f;
            }
        }
        else if (isWaiting)
        {
            timer += Time.deltaTime;

            if (timer >= delay)
            {
                isWaiting = false;
                PlayAnimation();
            }
        }
    }

    private void PlayAnimation()
    {
        timer = 0f;
        isPlaying = true;
    }
}
