﻿using System;
using Mirror;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MathD;
using UnityEngine.Serialization;

public class PlayerMovementController : NetworkBehaviour
{
    public enum PlayerMovementState
    {
        Crouch,
        Walk,
        Run
    }
    
    [SerializeField] private float movementSpeed = 5f;
    [SerializeField] private CharacterController controller = null;
    [SerializeField] private MonsterAI monster;
    [SerializeField] public float timePatience = 0f;
    [SerializeField] public float maxTimePatience = 5f;
    AudioSource m_MyAudioSource;
    Coroutine counterCry;
    private UIPlayer ui;
    private SoundManager soundManager;
    bool debugMod;
    public Vector3 movement;
    private float soundStepRangeCounter;
    private bool isWantCrouch;
    private bool isWantRun;

    [Space, Header("Sound gizmo")] public float LastShoutVolume;
    public float LastShoutTimer;
    
    private float SpeedStateMult
    {
        get
        {
            var speed = 1f;
            switch (movementState)
            {
                case PlayerMovementState.Crouch:
                    speed = 0.33f;
                    break;
                case PlayerMovementState.Walk:
                    speed = 1;
                    break;
                case PlayerMovementState.Run:
                    speed = 2f;
                    break;
            }
            return speed;
        } 
    }
    private float SoundStepRange
    {
        get
        {
            var step = 0.5f;
            switch (movementState)
            {
                case PlayerMovementState.Crouch:
                    step = 0.75f;
                    break;
                case PlayerMovementState.Walk:
                    step = 1.5f;
                    break;
                case PlayerMovementState.Run:
                    step = 3f;
                    break;
            }
            return step;
        } 
    }
    public PlayerMovementState movementState;

    public float MovementNoiseMult
    {
        get
        {
            var noiseMult = 1f;
            switch (movementState)
            {
                case PlayerMovementState.Crouch:
                    noiseMult = 0.33f;
                    break;
                case PlayerMovementState.Walk:
                    noiseMult = 1f;
                    break;
                case PlayerMovementState.Run:
                    noiseMult = 2f;
                    break;
            }
            return noiseMult;
        }
    }
    private NetworkManagerLobby managerLobby;
    private NetworkManagerLobby ManagerLobby
    {
        get
        {
            if (managerLobby != null) { return managerLobby; }
            return managerLobby = NetworkManager.singleton as NetworkManagerLobby;
        }
    }
    public override void OnStartAuthority()
    {
        enabled = true;
        ui = GetComponent<UIPlayer>();
        soundManager = GetComponent<SoundManager>();
        InputManager.Controls.Player.Cry.performed += ctx => Cry();
        InputManager.Controls.Player.DebugMode.performed += ctx => DebugMode();
        InputManager.Controls.Player.Crouch.performed += ctx => isWantCrouch = !isWantCrouch;
        movementState = PlayerMovementState.Walk;
        NetworkManagerLobby.OnCreateLocation += StartGame;
    }
    
    public void StartGame(NetworkConnection connection, bool _)
    {
        if (!isOwned) return;
        m_MyAudioSource = GetComponent<AudioSource>();
        monster = ManagerLobby.location.monster;
        counterCry = StartCoroutine(CounterCry());
    }

    void DebugMode()
    {
        debugMod = !debugMod;
    }

    IEnumerator CounterCry()
    {
        while (debugMod || timePatience < maxTimePatience)
        {
            timePatience += Time.deltaTime;
            yield return null;
        }

        Cry();
        yield return null;
        //Debug.Log("endCorutCry");
        counterCry = StartCoroutine(CounterCry());
    }

    void Cry()
    {
        timePatience = 0;
        CmdCry(netIdentity.connectionToClient);
    }

    [Command]
    void CmdCry(NetworkConnectionToClient connection)
    {
        ManagerLobby.location.UnlockMonsterRoom();
        RpcCry(connection);
        ManagerLobby.location.monster.AddPointInteresting(connection.identity.transform.position);
    }

    [TargetRpc]
    void RpcCry(NetworkConnectionToClient connection)
    {
        connection.identity.GetComponent<AudioSource>().Play();
    }

    [ClientCallback]
    private void Update()
    {
        if (!isOwned && !ManagerLobby.location.ready) return;
        
        if (isWantCrouch)
            movementState = PlayerMovementState.Crouch;
        else if (isWantRun)
            movementState = PlayerMovementState.Run;
        else
            movementState = PlayerMovementState.Walk;

        Move();
        if (ManagerLobby.currentState == NetworkManagerLobby.ManagerState.Game)
        {
            if (monster == null)
                monster = ManagerLobby.location?.monster;

            ui.StressScale.value = timePatience / maxTimePatience;
            if (soundStepRangeCounter >= SoundStepRange)
            {
                var volume = ui.SoundVolumeScale.highValue * MovementNoiseMult;
                LastShoutVolume = volume;
                ManagerLobby.location?.monster?.MakeSound(transform.position, volume);
                soundManager.AddAmplitude((int)(volume / 2f));
                soundStepRangeCounter = 0;
            }
        }
        else
        {
            
        }
        
       
        //var angle = Vector3.Angle(enemy.transform.forward, transform.position - enemy.transform.position);
        //if (angle <= 45)
        //{
        //    if (Physics.Raycast(enemy.transform.position, transform.position - enemy.transform.position))
        //    {
        //        enemy.AddTarget(target);
        //    }
        //}
    }

    [Client]
    private void Move()
    {
        var animator = GetComponentInChildren<Animator>();
        var input = InputManager.Controls.Player.Move.ReadValue<Vector2>();
        
        Vector3 right = controller.transform.right;
        Vector3 forward = controller.transform.forward;

        movement = right.normalized * input.x + forward.normalized * input.y;
        var gravityDirection = Physics.gravity * Time.deltaTime;
        //transform.position = new Vector3(transform.position.x, 0, transform.position.z);
        animator.SetFloat("MoveDirectionX", input.x * 2);//перенести в command
        animator.SetFloat("MoveDirectionZ", input.y * 2);
        controller.Move(movement * movementSpeed * SpeedStateMult * Time.deltaTime + gravityDirection);
        soundStepRangeCounter += movement.magnitude * movementSpeed * SpeedStateMult * Time.deltaTime;
    }

    private void OnDrawGizmos()
    {
        if (LastShoutVolume > 0)
        {
            LastShoutTimer += Time.deltaTime;
            var time = LastShoutVolume / 330f * 2.5f;
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(transform.position, LastShoutVolume * (LastShoutTimer / time));
            if (LastShoutTimer / time >= 1)
            {
                LastShoutTimer = 0;
                LastShoutVolume = 0;
            }
        }
    }
}

