using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "InventoryData", menuName = "Inventory")]
public class InventoryData : ScriptableObject
{
    public int countSlots;
}
