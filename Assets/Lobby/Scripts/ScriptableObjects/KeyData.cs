using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "KeyData", menuName = "Key")]
public class KeyData : ItemData
{
    public uint idKey;
}
