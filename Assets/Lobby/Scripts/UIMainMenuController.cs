using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.UIElements;
using Visibility = UnityEngine.UIElements.Visibility;

public class UIMainMenuController : MonoBehaviour
{
    public UIDocument offlineMenu;
    public UIDocument onlineMenu;
    
    private VisualElement root;

    private VisualElement startMenu;
    private Button exitButton;
    private Button hostButton;
    private Button findButton;

    public VisualElement hostMenu;
    private TextField maxPlayersField;
    private Toggle onlyFriendToggle;
    private Button returnButton;
    private Button resumeButton;
    private NetworkManagerLobby managerLobby;
    // Start is called before the first frame update
    void Start()
    {
        managerLobby = NetworkManager.singleton as NetworkManagerLobby;
        root = offlineMenu.GetComponent<UIDocument>().rootVisualElement;

        startMenu = root.Q<VisualElement>("Menu");
        exitButton = root.Q<Button>("ExitButton");
        hostButton = root.Q<Button>("HostButton");
        findButton = root.Q<Button>("FindGameButton");
        
        hostMenu = root.Q<VisualElement>("HostMenu");
        maxPlayersField = root.Q<TextField>("MaxPlayersField");
        onlyFriendToggle = root.Q<Toggle>("OnlyFriendToggle");
        returnButton = root.Q<Button>("ReturnButton");
        resumeButton = root.Q<Button>("ResumeButton");
        
        var findMenu = root.Q<VisualElement>("FindMenu");
        
        exitButton.RegisterCallback<ClickEvent>(_ => Application.Quit());
        hostButton.RegisterCallback<ClickEvent>(_ =>
        {
            startMenu.style.visibility = Visibility.Hidden;
            hostMenu.style.visibility = Visibility.Visible;
        });
        findButton.RegisterCallback<ClickEvent>(_ =>
        {
            startMenu.style.visibility = Visibility.Hidden;
            findMenu.style.visibility = Visibility.Visible;
        });
        returnButton.RegisterCallback<ClickEvent>(_ =>
        {
            startMenu.style.visibility = Visibility.Visible;
            hostMenu.style.visibility = Visibility.Hidden;
        });
        resumeButton.RegisterCallback<ClickEvent>(_ =>
        {
            managerLobby.CreateLobbyButton();
        });
        
        //customPropertyDrawer.RegisterCallback<ChangeEvent<int>>((changeEvent) => AsyncDispatchPropertyChangedEvent());
        // uxmlSlider.RegisterCallback<ChangeEvent<int>>((evt) =>
        // {
        //     csharpSlider.value = evt.newValue;
        // });
    }
}
