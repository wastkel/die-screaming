﻿using Mirror;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;


public class NetworkManagerLobby : NetworkManager
{
    public SteamManager steamManager;
    [SerializeField] private int minPlayers = 1;
    [Scene] [SerializeField] private string menuScene = string.Empty;

    [Header("Maps")] [SerializeField] private int numberOfRounds = 1;
    [SerializeField] private MapSet mapSet;

    [Header("Game")] [SerializeField] private GameObject playerSpawnSystem;
    private GameObject playerSpawnSystemInstance;
    [SerializeField] private GameObject roundSystem;
    [HideInInspector] public LocationGenerator location;
    public GameObject monsterPrefab;

    [Header("Lobby")] [SerializeField] public MainMenu uI_MainMenu;

    private MapHandler mapHandler;
    
    public ManagerState currentState = ManagerState.LobbyOffline;

    public static event Action OnClientConnected;
    public static event Action OnClientDisconnected;
    public static event Action<NetworkConnection> OnServerReadied;
    public static event Action<NetworkConnection, bool> OnCreateLocation;
    public static event Action<NetworkConnectionToClient> OnPlayerJoin;
    public static event Action OnServerStopped;
    public static event Action OnClientReadyUp;
    public LobbyData lobbyDataPrefab;
    private LobbyData lobbyData;
    public LobbyData LobbyData
    {
        get
        {
            if (lobbyData == null)
                return lobbyData = LobbyData.instance;
            return lobbyData;
        }
    }

    [FormerlySerializedAs("uiController")] public UIMainMenuController uiMainMenuController;

    [SerializeField] private Camera cameraMain;
    public override void Awake()
    {
        base.Awake();
    }

    public override void Start()
    {
        base.Start();
        spawnPrefabs = Resources.LoadAll<GameObject>("SpawnablePrefabs").ToList();
        currentState = ManagerState.LobbyOffline;
        //StartHost();
    }

    public override void OnStartServer()
    {
        base.OnStartServer();
        playerSpawnSystemInstance = Instantiate(playerSpawnSystem);
        NetworkServer.Spawn(playerSpawnSystemInstance);
        lobbyData = Instantiate(lobbyDataPrefab);
        NetworkServer.Spawn(lobbyData.gameObject);
    }

    public override void OnStartHost()
    {
        base.OnStartHost();
    }

    public override void OnServerConnect(NetworkConnectionToClient conn)
    {
        base.OnServerConnect(conn);
    }

    public override void OnStartClient()
    {
        base.OnStartClient();
        foreach (var prefab in spawnPrefabs)
            NetworkClient.RegisterPrefab(prefab);
        uiMainMenuController.offlineMenu.gameObject.SetActive(false);
        uiMainMenuController.hostMenu.style.visibility = UnityEngine.UIElements.Visibility.Hidden;
    }

    public override void OnServerReady(NetworkConnectionToClient conn)
    {
        base.OnServerReady(conn);
        OnServerReadied?.Invoke(NetworkClient.connection);
    }

    public override void OnServerAddPlayer(NetworkConnectionToClient conn)
    {
        if (conn.identity != null)
        {
            Debug.LogError("There is already a player for this connection.");
            return;
        }

        var playerGO = Instantiate(playerPrefab);
        playerGO.name = $"{playerPrefab.name} [connId={conn.connectionId}]";;
        NetworkServer.AddPlayerForConnection(conn, playerGO);
        OnPlayerJoin?.Invoke(conn);
        var player = playerGO.GetComponent<Player>();
        LobbyData.AddPlayer(player.netIdentity);
        var isLeader = LobbyData.Players.Count == 0;
        player.isLeader = isLeader;
    }

    public override void OnClientConnect()
    {
        base.OnClientConnect();
        NetworkClient.AddPlayer();
        OnClientConnected?.Invoke();
    }

    public override void OnClientDisconnect()
    {
        base.OnClientDisconnect();

        OnClientDisconnected?.Invoke();
    }
    
    public override void OnServerDisconnect(NetworkConnectionToClient conn)
    {
        if (conn.identity != null)
        {
            if (currentState == ManagerState.LobbyOnline)
                NotifyPlayersOfReadyState(conn);
            LobbyData.RemovePlayer(conn.identity);
        }

        base.OnServerDisconnect(conn);
    }

    public override void OnStopServer()
    {
        OnServerStopped?.Invoke();
        currentState = ManagerState.LobbyOffline;
        LobbyData.Players.Clear();
    }

    public void NotifyPlayersOfReadyState(NetworkConnectionToClient conn)
    {
        conn.identity.GetComponent<Player>().ui.HandleReadyToStart(IsReadyToStart());
    }

    private bool IsReadyToStart()
    {
        foreach (var player in LobbyData.Players)
            if (!player.IsReady)
                return false;
        
        return true;
    }

    public void NotifyPlayersOfReadySpawnState()
    {
        // foreach (var player in Players)
        // {
        playerSpawnSystemInstance.GetComponent<PlayerSpawnSystem>().HandleReadyToSpawn(IsReadyToSpawn());
    }

    public bool IsReadyToSpawn()
    {
        //:TODO продумать
        //if (numPlayers < minPlayers) { return false; }

        foreach (var player in LobbyData.Players)
            if (!player.IsReadySpawn)
                return false;
        
        return true;
    }

    public void CreateLobbyButton()
    {
        steamManager.CreateLobbyButton();
    }

    public void ReadyUpButton()
    {
        OnClientReadyUp.Invoke();
    }

    public void StartGame()
    {
        // if (!IsReadyToStart())
        //     return;

        mapHandler = new MapHandler(mapSet, numberOfRounds);
        ServerChangeScene(mapHandler.NextMap);
    }
    
    public void CreateLocation()
    {
        //if (!IsReadyToStart()) { return; }
        OnCreateLocation?.Invoke(NetworkClient.connection, true);
    }

    public void LeaveLobby()
    {
        StopClient();
        cameraMain.gameObject.SetActive(true);
        uiMainMenuController.offlineMenu.gameObject.SetActive(true);
    }

    public override void ServerChangeScene(string newSceneName)
    {
        base.ServerChangeScene(newSceneName);
    }

    public override void OnClientSceneChanged()
    {
        base.OnClientSceneChanged();
        currentState = ManagerState.Game;
    }

    public override void OnServerSceneChanged(string sceneName)
    {
        //NetworkServer.Destroy(playerSpawnSystemInstance);
        playerSpawnSystemInstance = Instantiate(playerSpawnSystem);
        NetworkServer.Spawn(playerSpawnSystemInstance);
    }

    public enum ManagerState
    {
        LobbyOffline,
        LobbyOnline,
        Game
    }
}