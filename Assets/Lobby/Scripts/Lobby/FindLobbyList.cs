﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Mirror;
using UnityEngine;
using Steamworks;
using UnityEngine.UIElements;
using Visibility = UnityEngine.UIElements.Visibility;

public class FindLobbyList : MonoBehaviour
{
    public VisualTreeAsset listEntryTemplate;
    private ListView lobbyListView;
    [SerializeField] SteamLobby steamLobby;
    private SteamLobby[] lobbies;
    //private ;
    private Coroutine lobbyAwait;
    public SteamManager steamManager = null;
    private NetworkManagerLobby managerLobby;
    private Button returnButton;
    private Button refreshButton;
    private Button connectButton;
    protected Callback<LobbyMatchList_t> LobbyMatchList;

    private void Start()
    {
        managerLobby = NetworkManager.singleton as NetworkManagerLobby;
        steamManager = managerLobby.steamManager;
        
        var uiDocument = managerLobby.uiMainMenuController.offlineMenu;
        var root = uiDocument.rootVisualElement.Q<VisualElement>("Root");
        var mainMenu = root.Q<VisualElement>("Menu");
        var findMenu = root.Q<VisualElement>("FindMenu");
        lobbyListView = findMenu.Q<ListView>("LobbyList");
        returnButton = findMenu.Q<Button>("ReturnButton");
        refreshButton = findMenu.Q<Button>("RefreshButton");
        connectButton = findMenu.Q<Button>("ConnectButton");
        
        returnButton.clicked += () =>
        {
            findMenu.style.visibility = Visibility.Hidden;
            mainMenu.style.visibility = Visibility.Visible;
        };
        refreshButton.clicked += Refresh;
        connectButton.clicked += JoinToLobby;
        lobbyListView.onSelectionChange += OnLobbySelected;
        LobbyMatchList = Callback<LobbyMatchList_t>.Create(OnLobbyMatchList);
    }

    void FillLobbyList(ListView lobbyListView)
    {
        lobbyListView.makeItem = () =>
        {
            var newListEntry = listEntryTemplate.Instantiate();
            var newListEntryLogic = new LobbyListEntryController();
            newListEntry.userData = newListEntryLogic;
            newListEntryLogic.SetVisualElement(newListEntry);
            return newListEntry;
        };
        
        lobbyListView.bindItem = (item, index) =>
        {
            (item.userData as LobbyListEntryController).SetLobbyData(lobbies[index]);
        };
        
        lobbyListView.itemsSource = lobbies;
    }

    private void JoinToLobby()
    {
        SteamMatchmaking.JoinLobby(new CSteamID(((SteamLobby)lobbyListView.selectedItem).m_SteamID));
    }
    
    private void OnLobbySelected(IEnumerable<object> selectedItems)
    {
        connectButton.style.visibility = lobbyListView.selectedItem is SteamLobby ? Visibility.Visible : Visibility.Hidden;
        
        // // Get the currently selected item directly from the ListView
        // var selectedCharacter = CharacterList.selectedItem as CharacterData;
        //
        // // Handle none-selection (Escape to deselect everything)
        // if (selectedCharacter == null)
        // {
        //     // Clear
        //     CharClassLabel.text = "";
        //     CharNameLabel.text = "";
        //     CharPortrait.style.backgroundImage = null;
        //
        //     return;
        // }
        //
        // // Fill in character details
        // CharClassLabel.text = selectedCharacter.Class.ToString();
        // CharNameLabel.text = selectedCharacter.CharacterName;
        // CharPortrait.style.backgroundImage = new StyleBackground(selectedCharacter.PortraitImage);
    }

    public void ClearList()
    {
        // int i = 0;
        // GameObject[] allChildren = new GameObject[content.transform.childCount];
        //
        // foreach (Transform child in content.transform)
        // {
        //     allChildren[i] = child.gameObject;
        //     i += 1;
        // }
        //
        // foreach (GameObject child in allChildren)
        // {
        //     DestroyImmediate(child.gameObject);
        // }
    }
    
    public void Refresh()
    {
        SteamMatchmaking.AddRequestLobbyListStringFilter(managerLobby.steamManager.GameName,managerLobby.steamManager.GameName,ELobbyComparison.k_ELobbyComparisonEqual);
        SteamMatchmaking.RequestLobbyList();
    }

    public void OnLobbyMatchList(LobbyMatchList_t callback)
    {
        lobbies = new SteamLobby[callback.m_nLobbiesMatching];
        for (var i = 0; i < callback.m_nLobbiesMatching; i++)
        {
            var lobby = SteamMatchmaking.GetLobbyByIndex(i);
            var name = SteamMatchmaking.GetLobbyData(lobby, "Name");
            lobbies[i] = new SteamLobby(lobby.m_SteamID, name, SteamMatchmaking.GetNumLobbyMembers(lobby));
        }
        
        FillLobbyList(lobbyListView);
        
    }
    
    // struct Lobby
    // {
    //     public CSteamID m_SteamID;
    //     public CSteamID m_Owner;
    //     public string m_OwnerName;
    //     public LobbyMembers[] m_Members;
    //     public int m_MemberLimit;
    //     public LobbyMetaData[] m_Data;
    // }
    //
    // struct LobbyMetaData
    // {
    //     public string m_Key;
    //     public string m_Value;
    // }
    //
    // struct LobbyMembers
    // {
    //     public CSteamID m_SteamID;
    //     public LobbyMetaData[] m_Data;
    // }

    private class LobbyListEntryController
    {
        private Label Name;
        private Label CurrentPlayers;
        private Label MaxPlayers;
        
        public void SetVisualElement(VisualElement visualElement)
        {
            Name = visualElement.Q<Label>("NameLabel");
            CurrentPlayers = visualElement.Q<Label>("CurrentPlayers");
            MaxPlayers = visualElement.Q<Label>("MaxPlayers");
        }
        public void SetLobbyData(SteamLobby lobbyData)
        {
            Name.text = lobbyData.Name;
            CurrentPlayers.text = lobbyData.MemberCount.ToString();
            MaxPlayers.text = "4";
        }
    }
}

public struct SteamLobby
{
    public ulong m_SteamID;
    public string Name;
    public int MemberCount;

    public SteamLobby(ulong id, string name, int memberCount)
    {
        m_SteamID = id;
        Name = name;
        MemberCount = memberCount;
    }
}
