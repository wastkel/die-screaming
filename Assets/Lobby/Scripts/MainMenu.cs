﻿using System;
using Mirror;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    public SteamManager steamManager = null;
    public LobbyHUD lobbyHUD;
    public FindLobbyList findLobbyList;
    private NetworkManagerLobby managerLobby;
    private void Awake()
    {
        managerLobby = NetworkManager.singleton as NetworkManagerLobby;
        steamManager = managerLobby.GetComponent<SteamManager>();
        //steamManager.uI_MainMenu = gameObject;
    }

    private void OnEnable()
    {
        managerLobby = NetworkManager.singleton as NetworkManagerLobby;
        steamManager = managerLobby.GetComponent<SteamManager>();
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}

