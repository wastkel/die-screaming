﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Steamworks;
using System;
using System.Collections;

public class PlayerNameInput : MonoBehaviour
{
    public static string DisplayName { get; private set; }
    [SerializeField] private string displayName;

    private const string PlayerPrefsNameKey = "PlayerName";

    private void Awake()
    {
        if (PlayerPrefs.HasKey(PlayerPrefsNameKey))
        {
            DisplayName = PlayerPrefs.GetString(PlayerPrefsNameKey);
            return;
        }

        StartCoroutine(WaitAutorization());
        SetUpInputField();
    }

    public void SetUpInputField()
    {
        if (PlayerPrefs.GetString(PlayerPrefsNameKey) == string.Empty)
            PlayerPrefs.DeleteKey(PlayerPrefsNameKey);

        if (PlayerPrefs.HasKey(PlayerPrefsNameKey))
            DisplayName = PlayerPrefs.GetString(PlayerPrefsNameKey);
    }

    public void SavePlayerName()
    {
        //DisplayName = SteamClient.Name;
        displayName = DisplayName;
        PlayerPrefs.SetString(PlayerPrefsNameKey, DisplayName);
    }

    IEnumerator WaitAutorization()
    {
        //while(!SteamManager.Initialized)
            yield return null;
        SavePlayerName();
    }
}

