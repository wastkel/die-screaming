using System;
using Mirror;
using Steamworks;
using UnityEngine;
using UnityEngine.Serialization;

public class Player : NetworkBehaviour
{
    [SerializeField] public Inventory inventory;
    [SerializeField] public UIPlayer ui;
    [SerializeField] private VoiceChat voiceChat;
    [SerializeField] public PlayerInteractionController interactionController;
    [SerializeField] public PlayerMovementController movementController;
    [SerializeField] public PlayerCameraController cameraController;
    [SerializeField] public CharacterController characterController;
    
    [SyncVar]
    public Texture2D displayAvatar;
    [SyncVar]
    public string displayName = "Loading...";
    [SyncVar]
    public bool IsReady = false;
    [SyncVar]
    public bool IsReadySpawn = false;
    [SyncVar]
    public bool isDead;
    private NetworkManagerLobby managerLobby => NetworkManager.singleton as NetworkManagerLobby;
    public bool isLeader;

    [ClientCallback]
    private void Update()
    {
        if (!isOwned) return;
        // switch (managerLobby.currentState)
        // {
        //     case NetworkManagerLobby.ManagerState.LobbyOnline:
        //         gameHUD.gameObject.SetActive(false);
        //         break;
        //     case NetworkManagerLobby.ManagerState.LobbyOffline:
        //         gameHUD.gameObject.SetActive(false);
        //         break;
        //     case NetworkManagerLobby.ManagerState.Game:
        //         gameHUD.gameObject.SetActive(true);
        //         break;
        // }
    }

    public void ReadyUpButton()
    {
        CmdReadyUp(connectionToClient);
    }

    public override void OnStartAuthority()
    {
        base.OnStartAuthority();
        enabled = true;
        ui = GetComponent<UIPlayer>();
        ui.player = this;
        cameraController = GetComponent<PlayerCameraController>();
        NetworkManagerLobby.OnClientReadyUp += ReadyUpButton;
        characterController = GetComponent<CharacterController>();
        movementController = GetComponent<PlayerMovementController>();
        interactionController = GetComponent<PlayerInteractionController>();
        inventory = GetComponent<Inventory>();
        
        int ret = SteamFriends.GetMediumFriendAvatar(SteamUser.GetSteamID());
        CmdSetDisplayName(SteamFriends.GetPersonaName(), GetSteamImageAsTexture2D(ret));
        ui.onlineMenu.gameObject.SetActive(true);
        ui.enabled = true;
        voiceChat = GetComponent<VoiceChat>();
        voiceChat.enabled = true;
        
    }

    public override void OnStartClient()
    {
        base.OnStartClient();
        DontDestroyOnLoad(gameObject);
        InputManager.Add(ActionMapNames.Player);
        InputManager.Controls.Player.Look.Enable();
        InputManager.Controls.Player.Cry.Enable();
        InputManager.Controls.Player.Move.Enable();
        InputManager.Controls.Player.Hotkey.Enable();
        InputManager.Controls.Player.Interaction.Enable();
        InputManager.Controls.Player.DebugMode.Enable();
        InputManager.Controls.Player.DropItem.Enable();
        InputManager.Controls.Player.SelectSlot.Enable();
        InputManager.Controls.Player.LCM.Enable();
        InputManager.Controls.Player.PCM.Enable();
        InputManager.Controls.Player.Crouch.Enable();
        InputManager.Controls.Player.VoiceChat.Enable();
    }

    private void AddPlayer(NetworkConnectionToClient conn)
    {
    }

    public override void OnStartServer()
    {
        base.OnStartServer();
    }

    public override void OnStopClient()
    {
        managerLobby.LobbyData.Players.Remove(this);
    }

    [Command]
    private void CmdSetDisplayName(string steamName, Texture2D steamAvatar)
    {
        displayName = steamName;
        displayAvatar = steamAvatar;
    }

    [Command]
    public void CmdReadyUp(NetworkConnectionToClient conn)
    {
        IsReady = !IsReady;
        if (conn == NetworkServer.localConnection)
            managerLobby.NotifyPlayersOfReadyState(conn);
    }

    [Command]
    public void CmdReadySpawn()
    {
        IsReadySpawn = !IsReadySpawn;
        managerLobby.NotifyPlayersOfReadySpawnState();
    }

    [Command]
    public void CmdStartGame()
    {
        if (managerLobby.LobbyData.Players[0].connectionToClient != connectionToClient)
        {
            return;
        }

        managerLobby.StartGame();
    }
    
    public static Texture2D GetSteamImageAsTexture2D(int iImage) 
    {
        Texture2D texture = null;
        uint imageWidth;
        uint imageHeight;
        bool isValid = SteamUtils.GetImageSize(iImage, out imageWidth, out imageHeight);

        if (isValid)
        {
            byte[] image = new byte[imageWidth * imageHeight * 4];
            isValid = SteamUtils.GetImageRGBA(iImage, image, (int)(imageWidth * imageHeight * 4));

            if (isValid)
            {
                texture = new Texture2D((int)imageWidth, (int)imageHeight, TextureFormat.RGBA32, false);
                texture.LoadRawTextureData(image);
                texture.Apply();
                texture = FlipTextureVertically(texture);
            }
        }
        return texture;
    }
    
    private static Texture2D FlipTextureVertically(Texture2D original)
    {
        Texture2D flipped = new Texture2D(original.width, original.height);
        for (int y = 0; y < original.height; y++)
        {
            for (int x = 0; x < original.width; x++)
            {
                flipped.SetPixel(x, original.height - y - 1, original.GetPixel(x, y));
            }
        }
        flipped.Apply();
        return flipped;
    }
}