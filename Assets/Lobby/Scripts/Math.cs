using UnityEngine;

namespace MathD
{
    public static class Math
    {
        public static float SqLength(Vector3 v)
        {
            return v.x * v.x + v.y * v.y + v.z * v.z;
        }
    }
    
    public static class Vector3Ext
    {
        public static Vector3 Vector3XZ(this Vector3 pos)
        {
            return new Vector3(pos.x, 0, pos.z);
        }
        public static Vector3 SetX(this Vector3 pos, float x)
        {
            return new Vector3(x, pos.y, pos.z);
        }
        public static Vector3 SetY(this Vector3 pos, float y)
        {
            return new Vector3(pos.x, y, pos.z);
        }
        public static Vector3 SetZ(this Vector3 pos, float z)
        {
            return new Vector3(pos.x, pos.y, z);
        }
    }
}

