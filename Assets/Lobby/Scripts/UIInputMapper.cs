using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;

public class UIInputMapper : MonoBehaviour
{
    private UIDocument document;
    public Camera cameraPlayer;
    
    
    private void OnEnable()
    {
        document = GetComponent<UIDocument>();
        document.panelSettings.SetScreenToPanelSpaceFunction((Vector2 screenPosition) =>
        {
            var invalidPosition = new Vector2(float.NaN, float.NaN);
            var cameraRay = cameraPlayer.ScreenPointToRay(new Vector2(Screen.width / 2,Screen.height / 2));
            Debug.DrawRay(cameraRay.origin, cameraRay.direction * 100, Color.magenta);
            RaycastHit hit;
            if (!Physics.Raycast(cameraRay, out hit, 100f,LayerMask.GetMask("UI")))
            {
                Debug.Log("invalidPos");
                return invalidPosition;
            }

            Vector2 pixelUV = hit.textureCoord;
            pixelUV.y = 1 - pixelUV.y;
            pixelUV.x *= document.panelSettings.targetTexture.width;
            pixelUV.y *= document.panelSettings.targetTexture.height;
            return pixelUV;
        });
    }
}
