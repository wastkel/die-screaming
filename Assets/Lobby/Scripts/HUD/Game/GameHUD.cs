using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameHUD : MonoBehaviour
{
    public GameObject cursorDot;
    public GameObject cursorInteract;
    private PlayerInteractionController interactionController;
    void Start()
    {
        interactionController = GetComponentInParent<PlayerInteractionController>();
    }

    private void Update()
    {
        if (interactionController)
        {
            if (interactionController.interactionObject == null)
            {
                cursorDot.SetActive(true);
                cursorInteract.SetActive(false);
            }
            else
            {
                cursorDot.SetActive(false);
                cursorInteract.SetActive(true);
            }
        }
    }
}
