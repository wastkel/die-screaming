using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Mirror;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;
using UnityEngine.UIElements;

public class Inventory : NetworkBehaviour
{
    [SerializeField] private List<ItemData> startItems = new ();
    public InventoryData inventoryData;
    [SerializeField] public ItemData[] inventoryItems;
    public UIPlayer ui;
    public int currentSlot = 0;
    [SerializeField] public Vector3 handsHook;
    private NetworkManagerLobby managerLobby;
    [SerializeField] public List<ItemData> itemPreset;
    private NetworkManagerLobby ManagerLobby
    {
        get
        {
            if (managerLobby == null)
                managerLobby = NetworkManager.singleton as NetworkManagerLobby;
            return managerLobby;
        }
    }
    
    private int CurrentSlot
    {
        get => currentSlot;
        set
        {
            currentSlot = value;
            if (currentSlot > 2)
                currentSlot = 0;

            if (currentSlot < 0)
                currentSlot = 2;
        }
    }
    
    private Controls controls;
    private Controls Controls
    {
        get
        {
            if (controls != null) { return controls; }
            return controls = new Controls();
        }
    }
    
    public override void OnStartAuthority()
    {
        enabled = true;
        Controls.Player.SelectSlot.performed += NextSlot;
        Controls.Player.DropItem.performed += ctx => DropItemFromInventory();
    }
    
    void Awake()
    {
        inventoryItems = new ItemData[3];
        for (int i = 0; i < startItems.Count; i++)
        {
            inventoryItems[i] = startItems[i];
        }
    }
    
    [ClientCallback]
    private void OnEnable() => Controls.Enable();
    [ClientCallback]
    private void OnDisable() => Controls.Disable();

    public void AddItemToInventory(InteractionItem item)
    {
        CmdAddItemToInventory(netIdentity, item.netId);
    }

    [Command(requiresAuthority = false)]
    void CmdAddItemToInventory(NetworkIdentity networkIdentity,uint itemId)
    {
        var interaction = NetworkServer.spawned[itemId].GetComponent<Interaction>(); //todo:  KeyNotFoundException The given key '120' was not present in the dictionary.
        switch (interaction)
        {
            case Key key:
                ManagerLobby.location.allKeys.Remove(key.netId);
                break;
            case Sheet sheet:
                ManagerLobby.location.allSheets.Remove(sheet.netId);
                break;
        }
        TargetAddItemToInventory(networkIdentity.connectionToClient,itemId);
    }
    
    [TargetRpc]
    void TargetAddItemToInventory(NetworkConnectionToClient  target,uint itemId)
    {
        var netInventory = GetComponent<Inventory>();
        
        var interaction = NetworkClient.spawned[itemId].GetComponent<Interaction>();
        switch (interaction)
        {
            case Key key:
                netInventory.inventoryItems[CurrentSlot] = key.keyData;
                break;
            case Sheet sheet:
                netInventory.inventoryItems[CurrentSlot] = sheet.itemData;
                break;
        }
        netInventory.ui.AddItem(CurrentSlot);
        DestroyServerObject(itemId);
    }
    
    public void DropItemFromInventory()
    {
        var indexItem = itemPreset.IndexOf(inventoryItems[currentSlot]);
        CmdDropItemInventory(netIdentity, indexItem);
    }
    
    [Command]
    void CmdDropItemInventory(NetworkIdentity playerIdentity, int indexItem)
    {
        var netTransform = playerIdentity.transform;
        var netInventory = playerIdentity.GetComponent<Inventory>();
        var item = Instantiate(itemPreset[indexItem].prefab, netTransform.rotation * netInventory.handsHook + netTransform.position + Vector3.up * 1.3f, Quaternion.identity);
        var velocity = playerIdentity.GetComponent<PlayerMovementController>().movement * 5 * Time.deltaTime;
        item.GetComponent<Rigidbody>().AddForce((netTransform.forward + velocity) * 100,ForceMode.Acceleration);
        NetworkServer.Spawn(item);
        var interaction = item.GetComponent<Interaction>();
        switch (interaction)
        {
            case Key key:
                netInventory.GetComponent<PlayerInteractionController>().keys.Remove(((KeyData)itemPreset[indexItem]).idKey);
                ManagerLobby.location.allKeys.Add(key.netId);
                break;
            case Sheet sheet:
                ManagerLobby.location.allSheets.Add(sheet.netId);
                break;
        }
        RpcRemoveItemFromInventory(playerIdentity);
    }
    
    [ClientRpc]
    void RpcRemoveItemFromInventory(NetworkIdentity playerIdentity)
    {
        var netInventory = playerIdentity.GetComponent<Inventory>();
        if (netInventory.inventoryItems[CurrentSlot] != null)
        {
            netInventory.inventoryItems[netInventory.CurrentSlot] = null;
            netInventory.ui.RemoveItem(netInventory.CurrentSlot);
        }
    }

    public void RemoveItemFromInventory()
    {
        if (inventoryItems[CurrentSlot] != null)
        {
            inventoryItems[CurrentSlot] = null;
            ui.RemoveItem(CurrentSlot);
        }
    }

    public void NextSlot(InputAction.CallbackContext context)
    {
        int direction = (int)context.ReadValue<Vector2>().y / 120;
        ui.Slots[CurrentSlot].style.height = 60; //60%
        CurrentSlot += direction;
        ui.Slots[CurrentSlot].style.height = 100; //100%
    }

    [Command(requiresAuthority = false)]
    void DestroyServerObject(uint id)
    {
        NetworkServer.Destroy(NetworkServer.spawned[id].gameObject);
    }
}
