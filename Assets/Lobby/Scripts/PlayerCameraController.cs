﻿using System;
using Cinemachine;
using Mirror;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;
using UnityEngine.UIElements;
using Cursor = UnityEngine.Cursor;


public class PlayerCameraController : NetworkBehaviour
    {
        [FormerlySerializedAs("сamera")]
        [Header("Camera")]
        [SerializeField] private Camera camera = null;
        [SerializeField] private float mouseSensitivity = 1;
        [SerializeField] private float speedCameraRotation = 1f;
        [SerializeField] private Player player;
        private UIDocument ui;
        public Button StarGameButton;
        public Button LeaveButton;
        
        private int currentCameraIndex;
        private int CurrentCameraIndex
        {
            get => currentCameraIndex;
            set
            {
                currentCameraIndex = value;
                var max = managerLobby.LobbyData.Players.Count - 1;
                if (currentCameraIndex > max)
                    currentCameraIndex = 0;
                if (currentCameraIndex < 0)
                    currentCameraIndex = max;
            }
        }
        float xRotation = 0f;
        private NetworkManagerLobby managerLobby;

        private UIDocument document;
    
        [ClientCallback]
        private void OnEnable()
        {
            Camera.main.gameObject.SetActive(false);
            InputManager.Controls.Enable();
            document = managerLobby.uiMainMenuController.onlineMenu.GetComponent<UIDocument>();
            document.panelSettings.SetScreenToPanelSpaceFunction((Vector2 screenPosition) =>
            {
                var invalidPosition = new Vector2(float.NaN, float.NaN);
                //var cameraRay = сamera.ScreenPointToRay(new Vector2(Screen.width / 2,Screen.height / 2));
                var cameraRay = camera.ScreenPointToRay(Mouse.current.position.ReadValue());
                Debug.DrawRay(cameraRay.origin, cameraRay.direction * 100, Color.magenta);
                RaycastHit hit;
                if (!Physics.Raycast(cameraRay, out hit, 100f,LayerMask.GetMask("UI")))
                    return invalidPosition;

                Vector2 pixelUV = hit.textureCoord;
                pixelUV.y = 1 - pixelUV.y;
                pixelUV.x *= document.panelSettings.targetTexture.width;
                pixelUV.y *= document.panelSettings.targetTexture.height;
                return pixelUV;
            });
        }
        
        public override void OnStartAuthority()
        {
            managerLobby = NetworkManager.singleton as NetworkManagerLobby;
            player = GetComponent<Player>();
            camera.gameObject.SetActive(true);
            if (managerLobby.currentState == NetworkManagerLobby.ManagerState.Game) return;
            ui = managerLobby.uiMainMenuController.onlineMenu;
            var root = ui.GetComponent<UIDocument>().rootVisualElement;
            StarGameButton = root.Q<Button>("StarGameButton");
            StarGameButton.clicked += () => managerLobby.StartGame();
            LeaveButton = root.Q<Button>("LeaveButton");
            LeaveButton.clicked += () => managerLobby.LeaveLobby();
            LeaveButton.clicked += () => player.ui.LeaveRoom();
            root.Q<Button>("ReadyButton").clicked += () => managerLobby.ReadyUpButton();
            
            root.Q<VisualElement>("Player1").Q<Button>("KickButton").clicked += () => Debug.Log("1");
            root.Q<VisualElement>("Player2").Q<Button>("KickButton").clicked += () => Debug.Log("2");
            root.Q<VisualElement>("Player3").Q<Button>("KickButton").clicked += () => Debug.Log("3");
            root.Q<VisualElement>("Player4").Q<Button>("KickButton").clicked += () => Debug.Log("4");
            
            Cursor.lockState = CursorLockMode.Locked;
            InputManager.Controls.Player.Look.performed += Look;
            InputManager.Controls.Player.LCM.performed += SwitchCamera;
            enabled = true;
        }
        
        [ClientCallback]
        private void OnDisable() => InputManager.Controls.Disable();
        
        private void Look(InputAction.CallbackContext context)
        {
            if (!isOwned) return;
            var lookAxis = context.ReadValue<Vector2>();
            float mouseX = lookAxis.x * mouseSensitivity * Time.deltaTime;
            float mouseY = lookAxis.y * mouseSensitivity * Time.deltaTime;
            xRotation -= mouseY;
            xRotation = Mathf.Clamp(xRotation, -90f, 90f);
            camera.transform.localRotation = Quaternion.Lerp(camera.transform.localRotation, Quaternion.Euler(xRotation, 0f, 0f), 0.5f);
            transform.Rotate(Vector3.Lerp(transform.forward, Vector3.up * mouseX, speedCameraRotation));
        }

        private void SwitchCamera(InputAction.CallbackContext context)
        {
            //todo:задел под спектатора
            // if (player.isDead)
            // {
            //     CurrentCameraIndex++;
            //     virtualCamera.gameObject.SetActive(false);
            // }
        }

        private void OnDestroy()
        {
            InputManager.Controls.Player.Look.performed -= Look;
            InputManager.Controls.Player.LCM.performed -= SwitchCamera;
        }
    }

