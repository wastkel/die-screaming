using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.UIElements;
using Visibility = UnityEngine.UIElements.Visibility;

public class UIPlayer : MonoBehaviour
{
    public UIDocument offlineMenu;
    public UIDocument onlineMenu;
    private NetworkManagerLobby managerLobby;
    private PlayerInteractionController interactionController;
    [SerializeField] private Inventory inventory;
    public Player player;
    public SoundManager soundManager;
    
    private VisualElement root;
    private VisualElement top;
    private VisualElement center;
    private VisualElement bot;
    private VisualElement party;
    public VisualElement cursor;
    public Texture2D dotIcon;
    public Texture2D handIcon;
    public Slider StressScale;
    public VisualElement Inventory;
    public SliderInt SoundVolumeScale;
    [SerializeField] private RectTransform itemsPanel;
    public VisualElement[] Slots = new VisualElement[3];

    private void Awake()
    {
        managerLobby = NetworkManager.singleton as NetworkManagerLobby;
    }

    private void Start()
    {
        interactionController = GetComponent<PlayerInteractionController>();
        soundManager = GetComponent<SoundManager>();
        
        root = onlineMenu.GetComponent<UIDocument>().rootVisualElement;
        
        top = root.Q<VisualElement>("Top");
        center = root.Q<VisualElement>("Center");
        bot = root.Q<VisualElement>("Bot");
        party = top.Q<VisualElement>("Party");
        cursor = root.Q<VisualElement>("RootCursor").Q<VisualElement>("Cursor");
        StressScale = bot.Q<Slider>("StressScale");
        Inventory = bot.Q<VisualElement>("Inventory");
        SoundVolumeScale = center.Q<SliderInt>("VolumeScale");
        
        inventory = GetComponent<Inventory>();
        inventory.ui = this;
        for (int i = 0; i < inventory.inventoryData.countSlots; i++)
        {
            Slots[i] = Inventory.Q<VisualElement>($"InventorySlot{i + 1}");
            var item = inventory.inventoryItems[i];
            if (item != null)
                Slots[i].Q<VisualElement>("Image").style.backgroundImage = item.image.texture;
            else
                Slots[i].Q<VisualElement>("Image").style.backgroundImage = null;
        }
    }

    private void Update()
    {
        FillPlayersList();

        if (interactionController.interactionObject == null)
        {
            cursor.style.backgroundImage = dotIcon;
            cursor.style.width = 8;
            cursor.style.height = 8;
        }
        else
        {
            cursor.style.backgroundImage = handIcon;
            cursor.style.width = 150;
            cursor.style.height = 150;
        }
    }

    void FillPlayersList()
    {
        for (var index = 0; index < managerLobby.LobbyData.Players.Count; index++)
        {
            var player = managerLobby.LobbyData.Players[index];
            var playerVisualElement = party.Q<VisualElement>($"PlayerTemplate{index + 1}");
            playerVisualElement.Q<VisualElement>("Root").Q<VisualElement>("Avatar").style.backgroundImage = player.displayAvatar;
            playerVisualElement.Q<VisualElement>("Root").Q<Label>("Name").text = player.displayName;
            playerVisualElement.style.visibility = Visibility.Visible;
        }
    }
    
    public void AddItem(int indexSlot)
    {
        Slots[indexSlot].Q<VisualElement>("Image").style.backgroundImage = inventory.inventoryItems[indexSlot].image.texture;
    }

    public void RemoveItem(int indexSlot)
    {
        Slots[indexSlot].Q<VisualElement>("Image").style.backgroundImage = null;
    }
    
    public void HandleReadyToStart(bool readyToStart)
    {
        if (!player.isLeader) { return; }

        player.cameraController.StarGameButton.visible = true;
        player.cameraController.StarGameButton.focusable = readyToStart;
    }

    public void LeaveRoom()
    {
        if(NetworkServer.active && NetworkClient.isConnected)
            managerLobby.StopHost();
        else if (NetworkClient.isConnected)
            managerLobby.StopClient();
    }
}
