using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.InputSystem;

public class RitePlace : Interaction
{
    public LocationGenerator locationGenerator;
    public float holdDuration = 0f;
    public Coroutine holdAction;
    public List<GameObject> exitPoints;

    public void AcceptSheet()
    {
        holdAction = StartCoroutine(StartAccept());
    }
    
    IEnumerator StartAccept()
    {
        var currentTime = 0f;

        while (currentTime <= holdDuration && InputManager.Controls.Player.Interaction.phase == InputActionPhase.Performed)
        {
            currentTime += Time.fixedUnscaledDeltaTime;
            yield return null;
        }

        if (currentTime >= holdDuration)
            if (++locationGenerator.currentAcceptedSheet >= locationGenerator.minRequiredSheets)
                locationGenerator.EscapeRite();
    }

    public void ChangeState()
    {
        var managerLobby = NetworkManager.singleton as NetworkManagerLobby;
        for (int i = 0; i < managerLobby.LobbyData.Players.Count; i++)
            exitPoints[i].SetActive(true);
    }
}
