using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEditor;
using UnityEngine.Serialization;

public class Door : MonoBehaviour , IInteraction
{
    public bool open;
    private Coroutine activeDoor = null;
    public float timeToOpen = 20;
    private NavMeshObstacle obstacle;
    [HideInInspector] public RoomManager roomManager;
    [SerializeField] private AudioClip ACTryOpenDoor;
    [SerializeField] private AudioClip ACUnlockDoor;
    private AudioSource audioSource;
    [NonSerialized] public float interactionRange = 2.5f;
    [NonSerialized] public float interactionRad = 0;

    public bool Locked
    {
        set
        {
            roomManager.lockedMainDoor = value;
            obstacle.carving = roomManager.lockedMainDoor;
        } 
        get => roomManager.lockedMainDoor;
    }

    private void Awake()
    {
        obstacle = GetComponentInChildren<NavMeshObstacle>();
        roomManager = GetComponentInParent<RoomManager>();
        audioSource = GetComponent<AudioSource>();
    }

    public void SetKey(Key key)
    {
        //idKey = key.itemData.idKey;
    }
    
    public bool TryUnlock(uint key)
    {
        if (roomManager.idKeyMainDoor == key)
        {
            audioSource.clip = ACUnlockDoor;
            audioSource.Play();
            obstacle.carving = false;
            Locked = false;
            return true;
        }
        else
        {
            audioSource.clip = ACTryOpenDoor;
            audioSource.Play();
            return false;
        }
    }
    
    public virtual void OpenDoor(Vector3 playerPos)
    {
        if (!Locked)
        {
            open = !open;
            activeDoor = StartCoroutine(ActiveDoor(playerPos));
        }
    }

    public virtual void OpenDoor(Vector3 playerPos, uint key)
    {
        if (Locked)
            if (!TryUnlock(key))
                return;

        open = !open;
        activeDoor = StartCoroutine(ActiveDoor(playerPos));  
    }

    IEnumerator ActiveDoor(Vector3 playerPos)
    {
        int requiredAngle;
        if (open)
            requiredAngle = -90;
        else
            requiredAngle = 0;
        var startRot = obstacle.transform.localRotation;
        var counter = 1 / timeToOpen;
        float timer = 0;

        while (timer <= 1)
        {
            timer += counter;
            obstacle.transform.localRotation = Quaternion.Lerp(startRot, Quaternion.Euler(new Vector3(0, requiredAngle, 0)), timer);
            yield return null;
        }
        activeDoor = null;
    }
}
