using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class DoorKey : NetworkBehaviour
{
    public int idKey;
    //public Key keyData;
    
    public int IdKey { get => idKey; }

    private void Start()
    {
        //keyData.idKey = idKey;
    }

    public void SetIdKey(int id)
    {
        idKey = id;
        //keyData.idKey = id;
    }
}
