using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.InputSystem;

public class RespawnSheet : Interaction
{
    [SyncVar] public NetworkIdentity currentPlayer;
    public float holdDuration = 0f;
    public Coroutine holdAction;
    private LocationGenerator location;
    public void RespawnPlayer()
    {
        if (!currentPlayer)
            return;

        holdAction = StartCoroutine(StartInteract());
        
    }

    IEnumerator StartInteract()
    {
        var currentTime = 0f;

        while (currentTime <= holdDuration && InputManager.Controls.Player.Interaction.phase == InputActionPhase.Performed)
        {
            currentTime += Time.fixedUnscaledDeltaTime;
            yield return null;
        }

        if (currentTime >= holdDuration)
        {
            //todo: респ на базе
            Respawn(currentPlayer.connectionToClient);
        }
    }

    [Server]
    public void Respawn(NetworkConnectionToClient conn)
    {
        var targetIdentity = conn.identity;
        targetIdentity.GetComponent<Player>().isDead = true;
        currentPlayer = null;
        gameObject.SetActive(false);
        TargetRespawn(conn);
        RpcRespawn(targetIdentity);
    }
    
    [ClientRpc]
    public void RpcRespawn(NetworkIdentity identity)
    {
        identity.gameObject.SetActive(true);
    }
    
    [TargetRpc]
    public void TargetRespawn(NetworkConnectionToClient conn)
    {
        var player = conn.identity.GetComponent<Player>();
        player.transform.position = gameObject.transform.forward;
        player.characterController.enabled = true;
        player.movementController.enabled = true;
        player.interactionController.enabled = true;
        player.inventory.enabled = true;
    }
}
