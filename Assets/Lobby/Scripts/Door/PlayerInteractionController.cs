using System.Collections;
using System.Collections.Generic;
using MathD;
using UnityEngine;
using Mirror;
using Unity.Mathematics;
using UnityEditor;
using UnityEngine.Serialization;

public class PlayerInteractionController : NetworkBehaviour
{
    RaycastHit hit;
    Camera camera;
    bool doorAvailable;
    public IInteraction interactionObject;

    Key keyObj;

    //private InteractionItem item;
    bool keyAvailable;
    public SyncList<uint> keys = new ();
    private Inventory inventory;
    private NetworkManagerLobby manager;
    [SerializeField] private LayerMask interactionLayerMask;
    
    public override void OnStartAuthority()
    {
        enabled = true;
        keys = new SyncList<uint>();
        inventory = GetComponent<Inventory>();
        camera = GetComponentInChildren<Camera>();
        manager = NetworkManager.singleton as NetworkManagerLobby;
        InputManager.Controls.Player.Interaction.performed += _ => Interact();
    }

    [ClientCallback]
    private void Update()
    {
        if (!isOwned) return;
        interactionObject = null;

        if (manager.location == null || !manager.location.ready)
            return;
        
        foreach (var id in manager.location.ritePlaces)
        {
            if (NetworkClient.spawned.TryGetValue(id, out NetworkIdentity networkIdentity))
                if (networkIdentity.TryGetComponent<RitePlace>(out var ritePlace))
                    foreach (var exit in ritePlace.exitPoints)
                    {
                        if (!exit.activeSelf) continue;
                        var position = transform.position;
                        var direction = position - exit.transform.position.SetY(position.y);
                        var sqLength = MathD.Math.SqLength(direction);
                        if (sqLength > 0.5f * 0.5f) continue;
                        Debug.Log("Евакуация");
                        gameObject.SetActive(false);
                    }
        }

        foreach (var id in manager.location.allRooms)
        {
            if (NetworkClient.spawned.TryGetValue(id, out NetworkIdentity networkIdentity))
                if (networkIdentity.TryGetComponent<RoomManager>(out var room))
                {
                    if (room.mainDoor != null)
                        if (AvailableInteract(room.mainDoor))
                            return;

                    if (room.currentPreset != null)
                        foreach (var containerDoor in room.currentPreset.containerDoors)
                            if (containerDoor != null)
                                if (AvailableInteract(containerDoor))
                                    return;
                }
        }

        foreach (var id in manager.location.allKeys)
        {
            if (NetworkClient.spawned.TryGetValue(id, out NetworkIdentity networkIdentity))
                if (networkIdentity.TryGetComponent<Key>(out var key))
                    if (AvailableInteract(key))
                        return;
        }

        foreach (var id in manager.location.allSheets)
        {
            if (NetworkClient.spawned.TryGetValue(id, out NetworkIdentity networkIdentity))
                if (networkIdentity.TryGetComponent<Sheet>(out var sheet))
                    if (AvailableInteract(sheet))
                        return;
        }

        if (manager.location.ritePlaces != null)
            foreach (var id in manager.location.ritePlaces)
            {
                var currentSlotItem = inventory.inventoryItems[inventory.currentSlot];
                if (currentSlotItem?.prefab&& currentSlotItem.prefab.TryGetComponent(out Sheet sheet))
                    if (NetworkClient.spawned.TryGetValue(id, out NetworkIdentity networkIdentity))
                        if (networkIdentity.TryGetComponent<RitePlace>(out var ritePlace))
                            if (AvailableInteract(ritePlace))
                                return;
            }

        foreach (var id in manager.location.respawns)
        {
            if (NetworkClient.spawned.TryGetValue(id, out NetworkIdentity networkIdentity))
                if (networkIdentity.TryGetComponent<RespawnSheet>(out var respawnSheet))
                    if (respawnSheet.currentPlayer != null && AvailableInteract(respawnSheet))
                        return;
        }
    }

    private bool AvailableInteract(Interaction obj)
    {
        var position = transform.position;
        var direction = position - obj.transform.position.SetY(position.y);
        var sqLength = MathD.Math.SqLength(direction);
        if (!(sqLength > obj.interactionRange * obj.interactionRange))
        {
            var cosine = Vector3.Dot(direction.normalized, -transform.forward);
            if (cosine > obj.interactionRad)
            {
                if (Physics.Raycast(camera.transform.position, camera.transform.forward, out hit))
                {
                    var interactObj = obj is MainDoor ? hit.transform.parent : hit.transform;
                    if (interactObj == obj.transform && interactObj.TryGetComponent<Interaction>(out var hitObj))
                    {
                        interactionObject = obj;
                        return true;
                    }
                }
                // else if (interactionObject == null) //todo: доработка под радиус взаимодействия 
                // {
                //     interactionObject = obj;
                //     return true;
                // }
            }
        }

        return false;
    }

    private bool AvailableInteract(Door obj)
    {
        var position = transform.position;
        var direction = position - obj.transform.position.SetY(position.y);
        var sqLength = MathD.Math.SqLength(direction);
        if (!(sqLength > obj.interactionRange * obj.interactionRange))
        {
            var cosine = Vector3.Dot(direction.normalized, -transform.forward);
            if (cosine > obj.interactionRad)
            {
                if (Physics.Raycast(camera.transform.position, camera.transform.forward, out hit, obj.interactionRange, interactionLayerMask))
                {
                    var interactObj = obj is MainDoor ? hit.transform.parent : hit.transform;
                    if (interactObj == obj.transform && interactObj.TryGetComponent<Door>(out var hitObj))
                    {
                        interactionObject = obj;
                        return true;
                    }
                }
                // else if (interactionObject == null) //todo: доработка под радиус взаимодействия 
                // {
                //     interactionObject = obj;
                //     return true;
                // }
            }
        }

        return false;
    }

    private void Interact()
    {
        switch (interactionObject)
        {
            case Door door:
                OpenDoor(door);
                break;
            case InteractionItem item:
                GetItem(item);
                break;
            case RitePlace ritePlace:
                AcceptSheet(ritePlace); //todo: переписать инвентарь
                break;
            case RespawnSheet sheet:
                InteractRespawnSheet(sheet);
                break;
        }
    }

    #region GetItem

    private void GetItem(InteractionItem item)
    {
        if (item != null)
            CmdGetItem(netIdentity, item);
    }

    [Command]
    private void CmdGetItem(NetworkIdentity player, InteractionItem item)
    {
        TargetGetItem(player.connectionToClient, item);
    }

    [TargetRpc]
    void TargetGetItem(NetworkConnectionToClient player, InteractionItem item)
    {
        if (item is Key key)
            player.identity.GetComponent<PlayerInteractionController>().keys.Add(key.idKey);

        player.identity.GetComponent<Inventory>().AddItemToInventory(item);
    }

    #endregion

    #region OpenDoor

    void OpenDoor(Door door)
    {
        if (door)
        {
            var roomIdentity = door.roomManager.netIdentity;
            var indexDoor = door.roomManager.CurrentPreset.containerDoors.IndexOf(door);
            if (door.Locked)
            {
                bool success = false;
                foreach (var key in keys)
                {
                    if (key == door.roomManager.idKeyMainDoor)
                    {
                        if (door is MainDoor) CmdOpenDoor(netIdentity, roomIdentity, key);
                        else CmdOpenDoor(netIdentity, roomIdentity, indexDoor, key);
                        success = true;
                        break;
                    }
                }

                if (!success)
                    if (door is MainDoor) CmdOpenDoor(netIdentity, roomIdentity, 0);
                    else CmdOpenDoor(netIdentity, roomIdentity, indexDoor, 0);
            }
            else
            {
                if (door is MainDoor) CmdOpenDoor(netIdentity, roomIdentity, 0);
                else CmdOpenDoor(netIdentity, roomIdentity, indexDoor, 0);
            }
        }
    }

    [Command]
    public void CmdOpenDoor(NetworkIdentity player, NetworkIdentity room, int indexDoor, uint key)
    {
        RpcOpenDoor(player, room, indexDoor, key);
    }

    [ClientRpc]
    void RpcOpenDoor(NetworkIdentity player, NetworkIdentity roomIdentity, int indexDoor, uint key)
    {
        var roomManager = roomIdentity.GetComponent<RoomManager>();
        roomManager.currentPreset.containerDoors[indexDoor].OpenDoor(player.transform.position, key);
    }

    [Command]
    public void CmdOpenDoor(NetworkIdentity player, NetworkIdentity room, uint key)
    {
        RpcOpenDoor(player, room, key);
    }

    [ClientRpc]
    void RpcOpenDoor(NetworkIdentity player, NetworkIdentity roomIdentity, uint key)
    {
        roomIdentity.GetComponent<RoomManager>().mainDoor.OpenDoor(player.transform.position, key);
    }

    #endregion

    #region AcceptSheet

    void AcceptSheet(RitePlace ritePlace)
    {
        inventory.RemoveItemFromInventory();
        CmdAcceptSheet(ritePlace);
    }

    [Command]
    public void CmdAcceptSheet(RitePlace ritePlace)
    {
        RpcAcceptSheet(ritePlace);
    }

    [ClientRpc]
    void RpcAcceptSheet(RitePlace ritePlace)
    {
        ritePlace.AcceptSheet();
    }

    #endregion

    #region InteractRespawnSheet

    private void InteractRespawnSheet(RespawnSheet sheet)
    {
        CmdInteractRespawnSheet(sheet);
    }

    [Command]
    private void CmdInteractRespawnSheet(RespawnSheet sheet)
    {
        RpcInteractRespawnSheet(sheet);
    }

    [ClientRpc]
    private void RpcInteractRespawnSheet(RespawnSheet sheet)
    {
        sheet.RespawnPlayer();
    }

    #endregion
}