using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Mirror;
using UnityEditor;
using UnityEngine.Serialization;

public abstract class Interaction : NetworkBehaviour, IInteraction
{
    [NonSerialized] public float interactionRange = 2.5f;
    [NonSerialized] public float interactionRad = 0;
}
