using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.Serialization;

public class ContainerDoor : Door
{
    [SerializeField] private InteractionDoorType doorType;
    private Coroutine activeDoor = null;
    [SerializeField] private float requiredAngle;
    [SerializeField] private Vector3 requiredDistance;
    
    public override void OpenDoor(Vector3 playerPos)
    {
        open = !open;
        activeDoor = StartCoroutine(ActiveDoor(playerPos));
    }

    public override void OpenDoor(Vector3 playerPos, uint key)
    {
        open = !open;
        activeDoor = StartCoroutine(ActiveDoor(playerPos));  
    }

    IEnumerator ActiveDoor(Vector3 playerPos)
    {
        switch (doorType)
        {
            case InteractionDoorType.Drawer:
            {
                var currentRequiredDistance = open ? requiredDistance : -requiredDistance;
                var startPos = transform.localPosition;
                var endPos = transform.localPosition + currentRequiredDistance;
                var counter = 1 / timeToOpen;
                float timer = 0;
                while (timer <= 1)
                {
                    timer += counter;
                    transform.localPosition = Vector3.Lerp(startPos, endPos, timer);
                    yield return null;
                }
                activeDoor = null;
                break;
            }
            case InteractionDoorType.Opening:
            {
                var currentRequiredAngle = open ? requiredAngle : 0f;
                var startRot = transform.localRotation;
                var counter = 1 / timeToOpen;
                float timer = 0;
                while (timer <= 1)
                {
                    timer += counter;
                    transform.localRotation = Quaternion.Lerp(startRot,
                        Quaternion.Euler(new Vector3(startRot.x, currentRequiredAngle, startRot.z)), timer);
                    yield return null;
                }
                activeDoor = null;
                break;
            }
        }
    }
    
    public enum InteractionDoorType
    {
        Drawer,
        Opening
    }
}
