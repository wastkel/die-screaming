using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.Serialization;

public class Key : InteractionItem
{
    public uint idKey;
    [FormerlySerializedAs("itemData")] public KeyData keyData;
    
     private void Awake()
     {
         idKey = keyData.idKey;
     }
}
