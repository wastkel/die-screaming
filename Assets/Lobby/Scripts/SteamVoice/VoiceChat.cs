using System;
using System.IO;
using Mirror;
using Steamworks;
using UnityEngine;
using UnityEngine.Serialization;

public class VoiceChat : NetworkBehaviour
{
    [SerializeField]
    private AudioSource audioSource;
    private MemoryStream stream;
    private MemoryStream input;

    private int optimalRate;
    private int clipBufferSize;
    private float[] clipBuffer;

    private int playbackBuffer;
    private int dataPosition;
    private int dataReceived;
    private EVoiceResult result;
    private UIPlayer ui;
    private SoundManager soundManager;
    private float amplitude;
    public float microphoneSensitivity = 6f;

    private void Start()
    {
        optimalRate = 48000;
            //(int)SteamUser.GetVoiceOptimalSampleRate();
        clipBufferSize = optimalRate * 5;
        clipBuffer = new float[clipBufferSize];
        
        stream = new MemoryStream();
        input = new MemoryStream();

        audioSource = GetComponent<AudioSource>();
        audioSource.clip = AudioClip.Create("VoiceData", clipBufferSize, 1, optimalRate, true, OnAudioRead);
        audioSource.loop = true;
        audioSource.Play();
        ui = GetComponent<UIPlayer>();
        soundManager = GetComponent<SoundManager>();
    }

    [ClientCallback]
    private void Update()
    {
        if (InputManager.Controls.Player.VoiceChat.IsPressed())
            SteamUser.StartVoiceRecording();
        else
            SteamUser.StopVoiceRecording();

        result = SteamUser.GetAvailableVoice(out var compressedBytes);
        if (result == EVoiceResult.k_EVoiceResultOK && compressedBytes > 0)
        {
            var compressedData = new byte[compressedBytes];
            result = SteamUser.GetVoice(true, compressedData, (uint)compressedData.Length, out var bytesWritten);
            if (result == EVoiceResult.k_EVoiceResultOK && bytesWritten > 0)
            {
                stream.Position = 0;
                stream.Write(compressedData, 0, (int)bytesWritten);
                CmdVoice(compressedData, bytesWritten);
            }
            ui.SoundVolumeScale.value = (int)(clipBuffer[dataPosition] / ui.SoundVolumeScale.highValue);
        }
    }
    
    [Command]
    public void CmdVoice(byte[] compressedData, uint bytesWritten)
    {
        RpcVoiceData(compressedData, bytesWritten);
    }

    [ClientRpc]
    public void RpcVoiceData(byte[] compressedData, uint bytesWritten)
    {
        var uncompressedData = new byte[optimalRate * 2];
        input.Write(compressedData, 0, (int)bytesWritten);
        input.Position = 0;
        result = SteamUser.DecompressVoice(compressedData, bytesWritten, uncompressedData,
            (uint)uncompressedData.Length, out var uncompressedBytes, (uint)optimalRate);

        if (result == EVoiceResult.k_EVoiceResultOK && uncompressedBytes > 0)
        {
            WriteToClip(uncompressedData, uncompressedBytes);
        }
    }

    [Client]
    private void OnAudioRead(float[] data)
    {
        for (int i = 0; i < data.Length; ++i)
        {
            data[i] = 0;
            if (playbackBuffer > 0)
            {
                dataPosition = (dataPosition + 1) % clipBufferSize;
                data[i] = clipBuffer[dataPosition];
                playbackBuffer--;
            }
        }
    }

    [Client]
    private void WriteToClip(byte[] uncompressed, uint size)
    {
        var sum = 0f;
        for (int i = 0; i < size; i += 2)
        {
            var converted = (short)(uncompressed[i] | uncompressed[i + 1] << 8) / 32767.0f;
            sum += Mathf.Abs(converted);
            if (!isOwned)
            {
                clipBuffer[dataReceived] = converted;
                dataReceived = (dataReceived + 1) % clipBufferSize;
                playbackBuffer++;
            }
        }
        soundManager.AddAmplitude((int)(sum / size * ui.SoundVolumeScale.range * microphoneSensitivity));
    }
}