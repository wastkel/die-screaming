﻿
using System;
using Mirror;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

    public class PlayerSpawnSystem : NetworkBehaviour
    {
        [SerializeField] private GameObject playerPrefab = null;
        private static List<Transform> spawnPoints = new List<Transform>();

        private void Awake()
        {
        }

        public override void OnStartServer()
        {
            base.OnStartServer();
            //NetworkManagerLobby.OnServerReadied += SpawnPlayer;
            NetworkManagerLobby.OnPlayerJoin += FirstSpawnPlayer;
            NetworkManagerLobby.OnCreateLocation += SpawnPlayers;
        }
        
        public override void OnStartAuthority()
        {
            base.OnStartAuthority();
        }
        
        public override void OnStartClient()
        {
            base.OnStartClient();
        }
        
        public static void AddSpawnPoint(Transform transform)
        {
            spawnPoints.Add(transform);
            spawnPoints = spawnPoints.OrderBy(x => x.GetSiblingIndex()).ToList();
        }
        public static void RemoveSpawnPoint(Transform transform) => spawnPoints.Remove(transform);

        [ServerCallback]
        private void OnDestroy()
        {
            NetworkManagerLobby.OnPlayerJoin -= FirstSpawnPlayer;
            NetworkManagerLobby.OnCreateLocation -= SpawnPlayers;
        } 

        [Server]
        public void SpawnPlayers(NetworkConnection conn, bool all)
        {
            var lobbyManager = (NetworkManagerLobby)NetworkManager.singleton;
            var nextIndex = lobbyManager.LobbyData.Players.Count;
            Transform spawnPoint = spawnPoints.ElementAtOrDefault(nextIndex);

            if (spawnPoint == null)
            {
                Debug.LogError($"Missing spawn point for player {nextIndex}");
                return;
            }

            if (all)
                for (var index = 0; index < lobbyManager.LobbyData.Players.Count; index++)
                    RpcTeleport(lobbyManager.LobbyData.Players[index].netIdentity);
            else
                RpcTeleport(conn.identity);
        }

        [Server]
        public void FirstSpawnPlayer(NetworkConnection conn)
        {
            RpcTeleport(conn.identity);
        }

        [ClientRpc]
        public void RpcTeleport(NetworkIdentity playerIdentity)
        {
            var lobbyManager = (NetworkManagerLobby)NetworkManager.singleton;
            var nextIndex = lobbyManager.LobbyData.Players.Count;
            playerIdentity.transform.position = spawnPoints[nextIndex].position;
            playerIdentity.transform.rotation = spawnPoints[nextIndex].rotation;
        }
        
        [Command]
        void CmdTeleport()
        {
            var lobbyManager = (NetworkManagerLobby)NetworkManager.singleton;
            for (var index = 0; index < lobbyManager.LobbyData.Players.Count; index++)
            {
                RpcTeleport(lobbyManager.LobbyData.Players[index].netIdentity);
            }
        }
        
        public void HandleReadyToSpawn(bool readyToStart)
        {
            if (isServer && readyToStart) { SpawnPlayers(connectionToServer, true); return; }
            // var lobbyManager = (NetworkManagerLobby)NetworkManager.singleton;
            // if (!lobbyManager.IsReadyToSpawn()) { return; }
            if (readyToStart)
                CmdTeleport();
        }
    }

